<?php

namespace MTi;

use RuntimeException;


class ItemNotFound
    extends RuntimeException
{
}
