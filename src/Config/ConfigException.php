<?php

namespace MTi\Config;

use LogicException;


class ConfigException
    extends LogicException
{
    public function __construct($file, $msg)
    {
        parent::__construct("Error in config file '$file': " . $msg);
    }
}
