<?php

namespace MTi\Config;

use InvalidArgumentException;
use MTi\Filesystem\FileNotFound;
use MTi\IConfig;
use MTi\Util\Arrays;


class IniConfig
    extends ConfigBase
{
    /**
     * ConfigIni constructor.
     * @param string $file
     * @param string|NULL $section
     * @throws FileNotFound
     */
    public function __construct(string $file, string $section = null)
    {
        $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        if ($extension != 'ini') {
            throw new ConfigFileExtensionNotSupported($file, $extension);
        }
        $this->file = $file;
        $this->setData($this->load($file, $section));
    }
    private $file;

    /**
     * @param string $file
     * @param string $section
     * @return array|mixed
     * @throws FileNotFound
     */
    private function load(string $file, string $section)
    {
        if (!is_file($file) || !is_readable($file)) {
            throw new FileNotFound($file);
        }
        $ini = parse_ini_file($file, true);
        $separator = trim(IConfig::SECTION_SEPARATOR);
        $data = array();
        foreach ($ini as $secName => $secData) {
            # is section?
            if (is_array($secData)) {
                if (substr($secName, -1) === IConfig::RAW_SECTION) {
                    $secName = substr($secName, 0, -1);
                } elseif (IConfig::KEY_SEPARATOR) {
                    // process key separators (key1> key2> key3)
                    $tmp = array();
                    foreach ($secData as $key => $val) {

                        $cursor = & $tmp;
                        foreach (explode(IConfig::KEY_SEPARATOR, $key) as $part) {
                            if (!isset($cursor[$part]) || is_array($cursor[$part])) {
                                $cursor = & $cursor[$part];
                            } else {
                                throw new ConfigInvalidKey($key, $secName, $file);
                            }
                        }
                        $cursor = $val;
                    }
                    $secData = $tmp;
                }
                // process extends sections like [staging < production] (with special support for separator ':')
                $parts = $separator
                    ? explode($separator, strtr($secName, ':', $separator))
                    : array($secName)
                ;
                if (count($parts) > 1) {
                    $parent = trim($parts[1]);
                    if (!isset($data[$parent]) || !is_array($data[$parent])) {
                        throw new ConfigInvalidParentSection($parent, $file);
                    }
                    $secData = array_reverse(
                        Arrays::mergeTree(
                            array_reverse($secData, true)
                          , array_reverse($data[$parent], true)
                        )
                      , true
                    );
                    $secName = trim($parts[0]);
                    if ($secName === '') {
                        throw new ConfigInvalidSection($file);
                    }
                }
            }
            if (false && IConfig::KEY_SEPARATOR) {
                $cursor = & $data;
                foreach (explode(IConfig::KEY_SEPARATOR, $secName) as $part) {
                    if (!isset($cursor[$part]) || is_array($cursor[$part])) {
                        $cursor = & $cursor[$part];
                    } else {
                        throw new ConfigInvalidSection($file, $secName);
                    }
                }
            } else {
                $cursor = & $data[$secName];
            }
            if (is_array($secData) && is_array($cursor)) {
                $secData = Arrays::mergeTree($secData, $cursor);
            }
            $cursor = $secData;
        }
        $this->linearizeVars($data);
        $this->loadConstants();
        array_walk_recursive($data, [$this, 'expand']);
        if ($section) {
            if (!isset($data[$section]) || !is_array($data[$section])) {
                throw new ConfigSectionNotFound($section, $file);
            }
            $data = $data[$section];
        }
        return $data;
    }
    private $variables = [];

    private function linearizeVars(array &$data, string $keyPrefix = '')
    {
        foreach ($data as $key => $value) {
            $relKey = $keyPrefix
                ? $keyPrefix.'/'.$key
                : $key
            ;
            if (is_array($value)) {
                $this->linearizeVars($value, $relKey);
            } else {
                $this->variables[$relKey] = $value;
            }
        }
    }

    private function loadConstants()
    {
        $buf = get_defined_constants(true);
        $consts = $buf['user'];
        foreach ($consts as $c => $value) {
            $parts = explode('_', $c);
            $c = join('', array_map('ucfirst', array_map('strtolower', $parts)));
            $this->variables[$c] = $value;
        }
    }

    protected function expand(&$s)
    {
        # TODO: implement cyclic reference check
        if (!is_string($s)) {
            return;
        }
        $parts = preg_split('#%([\w-/]*)%#i', $s, -1, PREG_SPLIT_DELIM_CAPTURE);
        $res = '';
        foreach ($parts as $n => $part) {
            if ($n % 2 === 0) {
                $res .= $part;//str_replace('%', '%%', $part);
            }
            elseif ($part === '') {
                $res .= '%';//'%%';
            }
            else {
                try {
                    $val = Arrays::get($this->variables, $part);
                }
                catch (InvalidArgumentException $e) {
                    $res .= "%$part%";
                    continue;
                }
                $this->expand($val);
                if (strlen($part) + 2 === strlen($s)) {
                    if (is_array($val)) {
                        array_walk_recursive($val, array($this, 'expand'));
                    }
                    $s = $val;
                }
                if (!is_scalar($val)) {
                    throw new InvalidArgumentException(
                        "Unable to concatenate non-scalar parameter '$part' into '$s'."
                    );
                }
                $res .= $val;
            }
        }
        $s = $res;
    }
}
