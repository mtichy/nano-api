<?php

namespace MTi\Config;


class ConfigSectionNotFound
    extends ConfigException
{
    public function __construct($section, $file)
    {
        parent::__construct($file, "There is not section [$section].");
    }
}
