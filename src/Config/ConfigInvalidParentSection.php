<?php

namespace MTi\Config;


class ConfigInvalidParentSection
    extends ConfigException
{
    public function __construct($section, $file)
    {
        parent::__construct($file, "Missing parent section [$section]");
    }
}
