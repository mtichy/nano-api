<?php

namespace MTi\Config;

use MTi\Config\Yaml\YamlCascadeImportsLoader;
use RuntimeException;
use Symfony\Component\Config\FileLocator;
use Yosymfony\ConfigLoader\Config;


class YamlConfig
    extends DotCascadedConfig
{

    /**
     * ConfigYaml constructor.
     * @param string $file
     * @param bool $ignoreMissingImports
     * @param bool $ignoreMissingConfigValues
     * @throws RuntimeException
     */
    public function __construct(
        string $file,
        bool $ignoreMissingImports = FALSE,
        bool $ignoreMissingConfigValues = FALSE
    )
    {
        $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        if ($extension != 'yml') {
            throw new ConfigFileExtensionNotSupported($file, $extension);
        }
        $locator = new FileLocator([dirname($file)]);
        $config = new Config([new YamlCascadeImportsLoader($locator, $file, $ignoreMissingImports)]);
        $data = $config->load(basename($file));
        $data = YamlExpander::expandArrayProperties($data->getArray());
        if (!$ignoreMissingConfigValues) {
            $this->checkRequired($data);
        }
        parent::__construct($data, $file);
    }

    /**
     * @param array $data
     * @param string $path
     * @throws MissingConfigItem
     */
    private function checkRequired(array $data, string $path = '')
    {
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                $this->checkRequired($val, $path . $key . '.');
            }
            else {
                if ($val === 'REQUIRED') {
                    throw new MissingConfigItem($path . $key);
                }
            }
        }
    }

    /**
     * @param array $data
     * @param string $path
     * @return array
     * @throws MissingConfigItem
     */
    public function findMissingRequired(array $data = NULL, string $path = '')
    {
        if (is_null($data)) {
            $data = $this->toArray();
        }
        $result = [];
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                $result = array_merge($result, $this->findMissingRequired($val, $path . $key . '.'));
            }
            else {
                if ($val === 'REQUIRED') {
                    $result[] = $path . $key;
                }
            }
        }
        return $result;
    }
}
