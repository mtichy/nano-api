<?php

namespace MTi\Config;


class MissingConfigItem
    extends ConfigException
{
    public function __construct($item, $file = NULL)
    {
        parent::__construct($file, "Missing config item '$item'.");
    }
}
