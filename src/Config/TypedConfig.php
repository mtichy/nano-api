<?php

namespace MTi\Config;


class TypedConfig
    extends ConfigBase
{
    public function __construct(array $cfgData)
    {
        $this->setData($cfgData);
    }
}
