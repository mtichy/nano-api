<?php

namespace MTi\Config;


class ConfigInvalidKey
    extends ConfigException
{
    public function __construct($key, $section, $file)
    {
        parent::__construct($file, "Invalid key '$key' in section [$section].");
    }
}
