<?php

namespace MTi\Config\Yaml;

use RuntimeException;
use Symfony\Component\Config\Exception\FileLoaderLoadException;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\Yaml\Yaml;
use Yosymfony\ConfigLoader\Loaders\YamlLoader;


class YamlCascadeImportsLoader
    extends YamlLoader
{
    /**
     * @var bool
     */
    private $ignoreMissingImports;
    /**
     * @var string
     */
    private $file;

    /**
     * Constructor.
     *
     * @param FileLocatorInterface $locator A FileLocatorInterface instance
     * @param string $file
     * @param bool $ignoreMissingImports
     */
    public function __construct(FileLocatorInterface $locator, string $file, bool $ignoreMissingImports = FALSE)
    {
        parent::__construct($locator);
        $this->ignoreMissingImports = $ignoreMissingImports;
        $this->file = $file;
    }

    public function load($resource, $type = NULL)
    {
        if (FALSE == class_exists('Symfony\Component\Yaml\Yaml')) {
            throw new RuntimeException('Symfony\Component\Yaml\Yaml parser is required to read yaml files.');
        }

        if (NULL === $type) {
            $resource = $this->getLocation($resource);
        }

        if ((!is_string($resource) || strlen($resource) < 4096) && is_file($resource)) {
            if (!is_readable($resource)) {
                throw new RuntimeException(sprintf('Unable to parse "%s" as the file is not readable.', $resource));
            }

            $resource = file_get_contents($resource);
        }
        $resource = str_replace('__CONFIG_DIR__', pathinfo($this->file, PATHINFO_DIRNAME), $resource);
        $data = Yaml::parse($resource, Yaml::PARSE_CONSTANT | Yaml::PARSE_OBJECT);
        $repository = new CascadeRepository();
        $repository->load($data ? $data : []);
        try {
            $data = $this->parseImports($repository, $resource);
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (FileLoaderLoadException $e) {
            if (!$this->ignoreMissingImports) {
                throw $e;
            }
            $data = $repository;
        }
        return $data;
    }
}
