<?php

namespace MTi\Config\Yaml;

use Yosymfony\ConfigLoader\Repository;
use Yosymfony\ConfigLoader\RepositoryInterface;


class CascadeRepository extends
    Repository
{
    /**
     * {@inheritdoc}
     */
    public function union(RepositoryInterface $repository)
    {
        $union = function (array $r1, array $r2) use (&$union) {
            $res = $r1;
            foreach (array_unique(array_merge(array_keys($r2), array_keys($r1))) as $k) {
                $v = $r2[$k] ?? NULL;
                if (isset($res[$k]) && is_array($r1[$k])) {
                    if (is_array($v)) {
                        $res[$k] = $union($r1[$k], $v);
                    }
                    else {
                        $res[$k] = $r1[$k];
                    }
                }
                else {
                    if (key_exists($k,$r2)) {
                        $res[$k] = $v;
                    }
                    else {
                        $res[$k] = $r1[$k];
                    }
                }
            }

            return $res;
        };

        $repo = new self();
        $repo->load($union($this->getArray(), $repository->getArray()));

        return $repo;
    }
}
