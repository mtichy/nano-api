<?php

namespace MTi\Config;

use MTi\IConfig;


abstract class ConfigBase
    implements IConfig
{
    protected function setData(array $cfgData)
    {
        $this->_data = $cfgData;
    }
    protected $_data = [];

    /**
     * {@inheritdoc}
     */
    public function config(): array
    {
        return $this->_data;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key, $default = null)
    {
        return array_key_exists($key, $this->_data)
            ? $this->_data[$key]
            : $default
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function need(string $key)
    {
        $val = $this->get($key);
        if (is_null($val)) {
            throw new MissingConfigItem(NULL,$key);
        }
        return $val;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return $this->_data;
    }
}
