<?php

namespace MTi\Config;


class DotCascadedConfig
    extends ConfigBase
{
    private $file;

    /**
     * DotCascadedConfig constructor.
     *
     * @param array $data
     * @param string $file
     */
    public function __construct(array $data, string $file = NULL)
    {
        $this->setData($data);
        $this->file = $file;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key, $default = NULL)
    {
        try {
            return $this->recursiveGet($key);
        }
        catch (ConfigException $ex) {
            return $default;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function need(string $key)
    {
        try {
            $val = parent::need($key);
        }
        catch (MissingConfigItem $e) {
            throw new MissingConfigItem($key, $this->file);
        }
        return $val;
    }

    private function recursiveGet(string $key, array $data = NULL, string $section = '')
    {
        if (is_null($data)) {
            $data = $this->toArray();
        }
        $parts = explode('.', $key, 2);
        if (count($parts) == 2) {
            $section .= $parts[0];
            if (array_key_exists($parts[0], $data)) {
                return $this->recursiveGet($parts[1], $data[$parts[0]]);
            }
            else {
                throw new ConfigSectionNotFound($section, $this->file);
            }
        }
        else {
            if (array_key_exists($parts[0], $data)) {
                return $data[$parts[0]];
            }
            else {
                throw new ConfigInvalidKey($parts[0], $section, $this->file);
            }
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, $value)
    {
        $this->recursiveSet($key, $value);
    }

    private function recursiveSet(string $key, $value, array &$data = NULL)
    {
        $root = FALSE;
        if (is_null($data)) {
            $root = TRUE;
            $data = $this->_data;
        }
        $parts = explode('.', $key, 2);
        if (count($parts) == 2) {
            if (array_key_exists($parts[0], $data)) {
                $data[$parts[0]] = $this->recursiveSet($parts[1], $value, $data[$parts[0]]);
            }
            else {
                $data[$parts[0]] = $value;
            }
        }
        else {
            $data[$parts[0]] = $value;
        }
        if ($root) {
            $this->_data = $data;
        }
        return $data;
    }
}
