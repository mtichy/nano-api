<?php

namespace MTi\Config;


class ConfigInvalidSection
    extends ConfigException
{
    public function __construct($file, $section = NULL)
    {
        parent::__construct(
            $file
          , "Invalid "
                . ($section
                    ? "section [$section]"
                    : "empty section name"
                )
        );
    }
}
