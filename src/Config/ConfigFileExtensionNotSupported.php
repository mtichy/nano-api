<?php

namespace MTi\Config;


class ConfigFileExtensionNotSupported
    extends ConfigException
{
    public function __construct($file, $extension)
    {
        parent::__construct($file, "Unsupported extension '$extension'.");
    }
}
