<?php

namespace MTi\Job;

use Cron\CronExpression;
use GO\Job;
use GO\Scheduler;
use Logger;
use MTi\IDatetime;
use MTi\IEnv;


abstract class BaseJob
{
    public function __construct(IEnv $ue, Scheduler $scheduler, string $script, CronExpression $cron)
    {
        $this->_ue = $ue;
        $this->_script = $script;
        $this->_sched = $scheduler;
        $this->_cron = $cron;
    }
    private $_script;
    private $_ue;
    private $_sched;
    private $_cron;

    private function env(): IEnv
    {
        return $this->_ue;
    }

    protected function logger(string $name): Logger
    {
        return $this->env()->logger($name);
    }

    abstract public function register(IDatetime $executionTime = NULL);

    protected function isLocking(): bool
    {
        return FALSE;
    }

    protected function connectJob(string $id = NULL, array $params = [], IDatetime $executionTime = NULL): ?Job
    {
        $executionTime = $executionTime
            ? $executionTime->asPhpDatetime()
            : $this->env()->currentDatetime()->asPhpDatetime()
        ;
        $cron = $this->_cron;
        if ($cron->isDue($executionTime)) {
            $job = $this->_sched->php($this->_script, NULL, $params, $id);
            $job->before([$this, 'before'])->then([$this, 'after'])->at($cron->getExpression());
            if ($this->isLocking()) {
                $job->onlyOne($this->env()->cacheDir() . '/scheduler');
            }
            return $job;
        }
        return NULL;
    }

    public function before(): void
    {
    }

    public function after($output, int $statusCode): void
    {
    }
}
