<?php

namespace MTi\Security;


interface IIdentity
{
    /**
     * Login name.
     *
     * @return string
     */
    public function networkName(): string;

    /**
     * Assigned role list
     *
     * @return array
     */
    public function roles(): array;

    /**
     * Sets a specific user attribute, for example "ID" in database storage.
     *
     * @param string $attributeName
     * @param mixed $value
     * @return IIdentity
     */
    public function setAttribute(string $attributeName, $value): IIdentity;

    /**
     * Get list of specific user attributes.
     *
     * @return array
     */
    public function getAttributes(): array;

    /**
     * Get specific user attribute value, if attribute exists.
     *
     * @param string $attributeName
     * @return mixed|null
     */
    public function getAttribute(string $attributeName);

    /**
     * Identifier of an active user session (if supported).
     *
     * @return string|null
     */
    public function getSessionId(): ?string;
}
