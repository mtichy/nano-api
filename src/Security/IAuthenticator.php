<?php

namespace MTi\Security;


interface IAuthenticator
{
    /**
     * Returns username of logged user account.
     *
     * @return string
     * @throws AuthenticationException Error during authentication process / session expired / explicit login needed
     */
    public function getUsername(): string;

    /**
     * Returns new session ID (if supported)
     *
     * @return string|null
     * @throws AuthenticationException Error during authentication process / session expired / explicit login needed
     */
    public function getSessionId(): ?string;

    /**
     * Tries authenticate user using username and password. In some cases returns specific output data.
     *
     * @param string $username
     * @param string $passphrase
     * @return array
     * @throws AuthenticationException
     */
    public function doExplicitLogin(string $username, string $passphrase): array;
}
