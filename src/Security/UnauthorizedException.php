<?php

namespace MTi\Security;

use LogicException;


class UnauthorizedException
    extends LogicException
{
}
