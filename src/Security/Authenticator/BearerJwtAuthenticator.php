<?php

namespace MTi\Security\Authenticator;

use Darsyn\IP\AbstractIP;
use Darsyn\IP\Exception\InvalidIpAddressException;
use Darsyn\IP\Exception\IpException;
use Darsyn\IP\Exception\WrongVersionException;
use Darsyn\IP\Version\Multi;
use DateInterval;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use LogicException;
use MTi\Application\SystemMisconfiguredException;
use MTi\Http\IRequest;
use MTi\IDatetime;
use MTi\Security\AuthenticationException;
use MTi\Security\IAuthenticator;
use MTi\Security\ICredentialsVerifier;


class BearerJwtAuthenticator
    implements IAuthenticator
{
    private $_username;

    /**
     * BearerJwtAuthMethod constructor.
     * @param IRequest $rq
     * @param ICredentialsVerifier $cv
     * @param string $secretKey Password for token encryption. In case of SSL it should be a private key (key file content)
     * @param IDatetime $now Token exposure date and time
     */
    public function __construct(IRequest $rq, ICredentialsVerifier $cv, string $secretKey, IDatetime $now)
    {
        $this->_request = $rq;
        $this->_credsVer = $cv;
        $this->_secret = $secretKey;
        $this->_now = $now;
    }
    private $_request;
    private $_credsVer;
    private $_secret;
    private $_now;
    private $_expire = NULL;
    private $_verifInternalIP = FALSE;
    private $_sessionId = NULL;

    public function setExpiration(DateInterval $i)
    {
        $this->_expire = $i;
    }

    public function setInternalIPVerificationOn()
    {
        $this->_verifInternalIP = TRUE;
    }

    private function getBearerToken()
    {
        $header = $this->_request->getAuthorizationHeader();
        if (!empty($header)) {
            if (preg_match('/Bearer\s(\S+)/', $header, $matches)) {
                return $matches[1];
            }
        }
        return NULL;
    }

    private function getAlgorithm(): string
    {
        return 'HS256';
    }

    private function checkTokenIP(AbstractIP $tokenIP): bool
    {
        $requestIP = $this->_request->getIP();
        if ($requestIP->getBinary() == $tokenIP->getBinary()) {
            return TRUE;
        }
        return !$this->_verifInternalIP
            && $requestIP->isPrivateUse()
            && $tokenIP->isPrivateUse()
        ;
    }

    private function parseUsernameFromToken(): string
    {
        $token = $this->getBearerToken();
        if (!$token) {
            throw new AuthenticationException(
                'Authentication token is missing.'
              , AuthenticationException::SECURITY
            );
        }
        try {
            $jwt = JWT::decode($token, $this->_secret, [$this->getAlgorithm()]);
        }
        catch (ExpiredException $e) {
            throw new AuthenticationException("Expired token.", AuthenticationException::EXPIRED_SESSION);
        }
        catch (Exception $e) {
            throw new AuthenticationException(
                sprintf("Invalid token (%s).", $e->getMessage())
              , AuthenticationException::SECURITY
            );
        }
        if (!isset($jwt->data)) {
            $invalidTokenReason = "missing data";
        }
        elseif (!isset($jwt->data->username)) {
            $invalidTokenReason = "missing username";
        }
        elseif (!isset($jwt->data->ip)) {
            $invalidTokenReason = "missing IP";
        }
        else {
            try {
                $ip = Multi::factory($jwt->data->ip);
                if (!$this->checkTokenIP($ip)) {
                    $invalidTokenReason = sprintf(
                        "IP mismatch: %s vs %s"
                      , $this->_request->getIP()->getDotAddress()
                      , $jwt->data->ip
                    );
                }
                else {
                    $invalidTokenReason = NULL;
                }
            }
            catch (WrongVersionException|InvalidIpAddressException $e) {
                $invalidTokenReason = 'invalid IP '.$jwt->data->ip;
            }
            catch (IpException $e) {
                throw new SystemMisconfiguredException('Invalid server IP '.$this->_request->getIP()->getBinary());
            }
        }
        if ($invalidTokenReason) {
            throw new AuthenticationException(
                sprintf("Invalid token (%s).", $invalidTokenReason)
              , AuthenticationException::SECURITY
            );
        }
        $this->_sessionId = strval($jwt->data->sessid);
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->_username = $jwt->data->username;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): string
    {
        if ($this->_username) {
            return $this->_username;
        }
        return $this->parseUsernameFromToken();
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId(): ?string
    {
        if ($this->_sessionId) {
            return $this->_sessionId;
        }
        return $this->parseUsernameFromToken();
    }

    /**
     * {@inheritdoc}
     */
    public function doExplicitLogin(string $username, string $passphrase): array
    {
        if (!$this->_credsVer->verifyCredentials($username, $passphrase)) {
            throw new AuthenticationException("Wrong credentials.", AuthenticationException::WRONG_CREDS);
        }
        $this->_username = $username;
        $this->_sessionId = uniqid();
        try {
            $ip = $this->_request->getIP()->getDotAddress();
        }
        catch (WrongVersionException|IpException $e) {
            throw new LogicException();
        }
        $tokenData = [
            'iat' => $this->_now->getTimestamp(),
            'nbf' => $this->_now->getTimestamp(),
            'iss' => 'NanoS',
            'data' => [
                'ip' => $ip,
                'login_dt' => $this->_now->isoDatetime(),
                'username' => $username,
                'sessid' => $this->getSessionId(),
            ],
        ];
        if ($this->_expire) {
            $exp = clone $this->_now;
            $tokenData['exp'] = $exp->add($this->_expire)->getTimestamp();
        }
        $jwt = JWT::encode($tokenData, $this->_secret, $this->getAlgorithm());
        return [
            'token' => $jwt,
        ];
    }
}
