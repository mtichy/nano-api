<?php

namespace MTi\Security\Authenticator;

use MTi\Http\HttpException;
use MTi\Http\IResponse;
use MTi\Security\AuthenticationException;
use MTi\Security\IAuthenticator;


class BasicAuthenticator
    implements IAuthenticator
{
    private $_username;

    /**
     * @param IResponse $response
     * @param string $realm BASIC realm
     * @param array $credentialsList Associative array of credentials [username => password]
     */
    public function __construct(IResponse $response, string $realm, array $credentialsList)
    {
        $this->_response = $response;
        $this->_realm = $realm;
        $this->_creds = $credentialsList;
    }
    private $_response;
    private $_realm;
    private $_creds;

    /**
     * @return string
     * @throws HttpException
     */
    private function auth(): string
    {
        if (empty($_SERVER['PHP_AUTH_USER'])) {
            $this->_response
                ->setCode(IResponse::S401_UNAUTHORIZED)
                ->setHeader('WWW-Authenticate', sprintf('Basic realm="%s"', $this->_realm))
            ;
            exit(0);
        }
        if (
            !array_key_exists($_SERVER['PHP_AUTH_USER'], $this->_creds)
            || $_SERVER['PHP_AUTH_PW'] != $this->_creds[$_SERVER['PHP_AUTH_USER']]
        ) {
            throw new AuthenticationException("Wrong credentials.", AuthenticationException::WRONG_CREDS);
        }
        return $this->_username = $_SERVER['PHP_AUTH_USER'];
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): string
    {
        if ($this->_username) {
            return $this->_username;
        }
        try {
            return $this->auth();
        }
        catch (HttpException $e) {
            throw new AuthenticationException('Headers sent.', 0, $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId(): ?string
    {
        return NULL;
    }

    /**
     * {@inheritdoc}
     */
    public function doExplicitLogin(string $username, string $passphrase): array
    {
        return []; // BASIC gets credentials strictly from $_SERVER variable
    }
}
