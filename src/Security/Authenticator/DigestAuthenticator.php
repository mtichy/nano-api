<?php

namespace MTi\Security\Authenticator;

use MTi\Http\HttpException;
use MTi\Http\IResponse;
use MTi\Security\AuthenticationException;
use MTi\Security\IAuthenticator;


class DigestAuthenticator
    implements IAuthenticator
{
    private $_username;

    /**
     * @param IResponse $response
     * @param string $realm Digest realm
     * @param array $credentialsList Associative array of credentials [username => password]
     */
    public function __construct(IResponse $response, string $realm, array $credentialsList)
    {
        $this->_response = $response;
        $this->_realm = $realm;
        $this->_creds = $credentialsList;
    }
    private $_response;
    private $_realm;
    private $_creds;

    /**
     * Function to parse the http auth header.
     *
     * @param $txt
     * @return array|bool
     */
    private function digestParse($txt)
    {
        // protect against missing data
        $needed_parts = array(
            'nonce' => 1,
            'nc' => 1,
            'cnonce' => 1,
            'qop' => 1,
            'username' => 1,
            'uri' => 1,
            'response' => 1,
        );
        $data = array();
        $keys = implode('|', array_keys($needed_parts));
        preg_match_all(
            '@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@'
            , $txt
            , $matches
            , PREG_SET_ORDER
        );
        foreach ($matches as $m) {
            $data[$m[1]] = $m[3] ? $m[3] : $m[4];
            unset($needed_parts[$m[1]]);
        }
        return $needed_parts ? FALSE : $data;
    }

    /**
     * @return string
     * @throws HttpException
     */
    private function auth(): string
    {
        if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
            $this->_response
                ->setCode(IResponse::S401_UNAUTHORIZED)
                ->setHeader(
                    'WWW-Authenticate'
                  , sprintf(
                        'Digest realm="%s",qop="auth",nonce="%s",opaque="%s"'
                      , $this->_realm
                      , uniqid()
                      , md5($this->_realm)
                    )
                )
            ;
            exit(0);
        }
        if (
            !($data = self::digestParse($_SERVER['PHP_AUTH_DIGEST']))
            || !array_key_exists($data['username'], $this->_creds)
        ) {
            throw new AuthenticationException("Wrong credentials.", AuthenticationException::WRONG_CREDS);
        }
        $A1 = md5($data['username'] . ':' . $this->_realm . ':' . $this->_creds[$data['username']]);
        $A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
        $valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);
        if ($data['response'] != $valid_response) {
            throw new AuthenticationException("Wrong credentials.", AuthenticationException::WRONG_CREDS);
        }
        return $this->_username = $data['username'];
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): string
    {
        if ($this->_username) {
            return $this->_username;
        }
        try {
            return $this->auth();
        }
        catch (HttpException $e) {
            throw new AuthenticationException('Headers sent.', 0, $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId(): ?string
    {
        return NULL;
    }

    /**
     * {@inheritdoc}
     */
    public function doExplicitLogin(string $username, string $passphrase): array
    {
        return []; // DIGEST gets credentials strictly from $_SERVER variable
    }
}
