<?php

namespace MTi\Security\Authenticator;

use MTi\Security\IAuthenticator;


class WelcomeAllAuthenticator
    implements IAuthenticator
{
    /**
     * {@inheritdoc}
     */
    public function getUsername(): string
    {
        return 'guest';
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId(): ?string
    {
        return NULL;
    }

    /**
     * {@inheritdoc}
     */
    public function doExplicitLogin(string $username, string $passphrase): array
    {
        return [];
    }
}
