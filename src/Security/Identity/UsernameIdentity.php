<?php

namespace MTi\Security\Identity;

use MTi\Security\IIdentity;


class UsernameIdentity
    implements IIdentity
{
    public function __construct(string $username, array $roles, string $sessionId = NULL)
    {
        $this->_username = $username;
        $this->_roles = $roles;
        $this->_sessId = $sessionId;
    }
    private $_username;
    private $_roles;
    private $_sessId;
    private $_attributes = [];

    /**
     * {@inheritdoc}
     */
    public function networkName(): string
    {
        return $this->_username;
    }

    /**
     * {@inheritdoc}
     */
    public function roles(): array
    {
        return $this->_roles;
    }

    /**
     * {@inheritdoc}
     */
    public function setAttribute(string $attributeName, $value): IIdentity
    {
        $this->_attributes[$attributeName] = $value;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->_attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttribute(string $attributeName)
    {
        return array_key_exists($attributeName, $this->_attributes)
            ? $this->_attributes[$attributeName]
            : NULL
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId(): ?string
    {
        return $this->_sessId;
    }
}
