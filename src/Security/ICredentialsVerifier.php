<?php

namespace MTi\Security;


interface ICredentialsVerifier
{
    /**
     * Verifies given credentials.
     *
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function verifyCredentials(string $username, string $password): bool;
}
