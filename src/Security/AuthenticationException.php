<?php

namespace MTi\Security;

use LogicException;


class AuthenticationException
    extends LogicException
{
    const WRONG_CREDS = -1;
    const NO_ROLE = -2;
    const EXPIRED_ACCOUNT = -3;
    const SECURITY = -4;
    const EXPIRED_SESSION = -5;
}
