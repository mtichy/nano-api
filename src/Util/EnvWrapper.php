<?php

namespace MTi\Util;

use MTi\IEnv;


abstract class EnvWrapper
{
    public function __construct(IEnv $e)
    {
        $this->_ue = $e;
    }
    private $_ue;

    final protected function env()
    {
        return $this->_ue;
    }

    abstract protected function before(): void;

    abstract protected function after(): void;

    /**
     * @param callable $cb
     * @param array $params
     * @return mixed
     */
    final public function execute(callable $cb, ...$params)
    {
        $this->before();
        $r = call_user_func_array($cb, $params);
        $this->after();
        return $r;
    }
}
