<?php

namespace MTi\Util;

use Doctrine\Common\Collections\Collection;
use JsonSerializable;
use MTi\IDate;
use MTi\IDatetime;
use MTi\NotSupportedException;
use MTi\Type\ExtendedFloat;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class MsExcel
{
    /**
     * @param array $data
     * @return Xlsx
     * @throws Exception
     */
    public static function arrayToXls(array &$data)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $writer = new Xlsx($spreadsheet);
        $row = 2;
        $cols = [NULL];
        foreach ($data as $key => &$val) {
            if ($val instanceof JsonSerializable) {
                $val = $val->jsonSerialize();
            }
            elseif ($val instanceof Collection) {
                $val = $val->getValues();
            }
            elseif (!(is_array($val) || is_scalar($val) || is_null($val))) {
                throw new NotSupportedException('Invalid data type');
            }
            if (is_array($val)) {
                foreach ($val as $key2 => &$val2) {
                    if (!in_array($key2, $cols)) {
                        $cols[] = $key2;
                    }
                    $colNumber = array_search($key2, $cols);
                    self::writeValue($sheet, $colNumber, $row, $val2);
                }
            }
            else {
                self::writeValue($sheet, 1, $row, $key);
                self::writeValue($sheet, 2, $row, $val);
            }
            $row++;
        }
        if (count($cols) == 1) {
            $cols[] = 'Property';
            $cols[] = 'Value';

            $sheet->getStyle("A1:A" . $sheet->getHighestDataRow())->getFont()->setBold(TRUE);
        }
        foreach ($cols as $colNumber => $val) {
            if ($colNumber > 0) {
                $sheet->setCellValueByColumnAndRow($colNumber, 1, $val);
                $sheet->getColumnDimensionByColumn($colNumber)->setWidth(min(30, max(9, strlen($val) + 2)));
            }
        }
        $sheet->getStyle("A1:" . $sheet->getHighestDataColumn() . "1")->getFont()
            ->setBold(TRUE)
            ->setSize(12)
            ->setItalic(TRUE)
        ;
        return $writer;
    }

    /**
     * @param Worksheet $sheet
     * @param int $col
     * @param int $row
     * @param mixed $val
     */
    private static function writeValue(Worksheet $sheet, int $col, int $row, $val): void
    {
        if (is_scalar($val)) {
            if (is_int($val)) {
                $sheet->setCellValueByColumnAndRow($col, $row, $val);
                $sheet->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_NUMBER)
                ;
            }
            elseif (is_float($val)) {
                $sheet->setCellValueByColumnAndRow($col, $row, $val);
                $sheet->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()
                    ->setFormatCode('# ##0.00')
                ;
            }
            elseif (is_bool($val)) {

                $sheet->setCellValueByColumnAndRow($col, $row, $val ? 'Ano' : 'Ne');
                $sheet->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()
                    ->setFormatCode('# ##0.00')
                ;
            }
            else {
                $sheet->setCellValueByColumnAndRow($col, $row, $val);
            }
        }
        elseif ($val instanceof IDate) {
            $sheet->setCellValueByColumnAndRow($col, $row, Date::dateTimeToExcel($val->asPhpDateTime()));
            $sheet->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()
                ->setFormatCode('d.mm.yyyy')
            ;
        }
        elseif ($val instanceof IDatetime) {
            $sheet->setCellValueByColumnAndRow($col, $row, Date::dateTimeToExcel($val->asPhpDatetime()));
            $sheet->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()
                ->setFormatCode('d.mm.yyyy hh:mm:ss')
            ;
        }
        elseif ($val instanceof ExtendedFloat) {
            $sheet->setCellValueByColumnAndRow($col, $row, $val->float());
            $sheet->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()
                ->setFormatCode('# ##0.00')
            ;
        }
        elseif (is_array($val) || $val instanceof JsonSerializable) {
            $sheet->setCellValueByColumnAndRow($col, $row, json_encode($val));
        }
    }
}
