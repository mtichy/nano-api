<?php

namespace MTi\Util;

use Nette\Loaders\RobotLoader;
use Symfony\Component\DependencyInjection\ContainerBuilder;


class Autoloader
{
    public static function autoloadDirs(array $dirs, string $cacheDir, ContainerBuilder $container = NULL)
    {
        $loader = new RobotLoader();
        if (!is_null($container)){
            $container->set('classLoader',$loader);
        }
        foreach ($dirs as $dir) {
            $loader->addDirectory($dir);
        }
        $loader->setTempDirectory($cacheDir);
        $loader->register();
    }
}
