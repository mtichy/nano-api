<?php

namespace MTi\Util;

use Doctrine\Common\Collections\Collection;
use JsonSerializable;
use Nette\Utils\JsonException;


class Json
{
    /**
     * @param array $data
     * @throws JsonException
     */
    public static function ensureJson(array &$data)
    {
        foreach ($data as &$val) {
            if ($val instanceof JsonSerializable) {
                $val = $val->jsonSerialize();
            }
            elseif (is_array($val)) {
                self::ensureJson($val);
            }
            elseif ($val instanceof Collection) {
                $val = $val->toArray();
            }
            elseif (!(is_scalar($val) || is_null($val))) {
                throw new JsonException();
            }
        }
    }
}
