<?php

namespace MTi\Util;

use LogicException;


class Pager
{
    public function __construct(int $page, int $limit)
    {
        $this->page = $page > 0 ? $page : 1;
        $this->limit = $limit;
    }
    private $page;
    private $limit;
    private $totalRows;

    public function setTotalRows(int $rows)
    {
        $this->totalRows = intval($rows);
        $this->page = min($this->pageCount(), $this->page);
    }
    public function page(): int
    {
        return $this->page;
    }

    public function totalRows(): int
    {
        if (is_null($this->totalRows)) {
            throw new LogicException('call setTotalRows() first');
        }
        return $this->totalRows;
    }
    public function pageCount(): int
    {
        return ceil($this->totalRows() / $this->limit);
    }

    public function firstRow(): int
    {
        return ($this->page - 1) * $this->limit + 1;
    }

    public function lastRow(): int
    {
        return min(
            $this->page * $this->limit
          , $this->totalRows()
        );
    }
    protected function limit(): int
    {
        return $this->limit;
    }
}
