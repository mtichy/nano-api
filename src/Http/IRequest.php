<?php

namespace MTi\Http;

use Darsyn\IP\AbstractIP;
use MTi\Input\IInputDataProvider;
use Nette\Http\Url;


interface IRequest
    extends IInputDataProvider
{
    public const CONTENT_TYPE_FORM = 'multipart/form-data';
    public const CONTENT_TYPE_XFORM = 'application/x-www-form-urlencoded';
    public const CONTENT_TYPE_JSON = 'application/json';

    /**
     * @return Url
     */
    public function getServerUrl(): Url;

    public function getRequestUrl(): Url;

    /**
     * @return string GET | POST | PUT | DELETE | OPTIONS
     */
    public function getMethod(): string;

    /**
     * @return string|null Declared type of received content
     */
    public function getContentType(): ?string;

    /**
     * @return AbstractIP
     */
    public function getIP(): AbstractIP;

    /**
     * @return string|null
     */
    public function getAuthorizationHeader(): ?string;
}
