<?php

namespace MTi\Http;

use MTi\IOException;


class HttpException
    extends IOException
{
}
