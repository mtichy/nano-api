<?php

namespace MTi\Http;


use InvalidArgumentException;
use JsonSerializable;
use MTi\IDatetime;


interface IResponse
{
    /** @var int cookie expiration: forever (23.1.2037) */
    public const COOKIE_EXPIRE_PERMANENT = 2116333333;

    /** @var int cookie expiration: until the browser is closed */
    public const COOKIE_EXPIRE_BROWSER = 0;

    /** HTTP 1.1 response code */
    public const S200_OK = 200;
    public const S204_NO_CONTENT = 204;
    public const S300_MULTIPLE_CHOICES = 300;
    public const S301_MOVED_PERMANENTLY = 301;
    public const S302_FOUND = 302;
    public const S303_SEE_OTHER = 303;
    public const S303_POST_GET = 303;
    public const S304_NOT_MODIFIED = 304;
    public const S307_TEMPORARY_REDIRECT= 307;
    public const S400_BAD_REQUEST = 400;
    public const S401_UNAUTHORIZED = 401;
    public const S403_FORBIDDEN = 403;
    public const S404_NOT_FOUND = 404;
    public const S405_METHOD_NOT_ALLOWED = 405;
    public const S406_NOT_ACCEPTABLE = 406;
    public const S408_REQUEST_TIMEOUT = 408;
    public const S409_CONFLICT = 409;
    public const S410_GONE = 410;
    public const S500_INTERNAL_SERVER_ERROR = 500;
    public const S501_NOT_IMPLEMENTED = 501;
    public const S503_SERVICE_UNAVAILABLE = 503;
    public const S505_HTTP_VERSION_NOT_SUPPORTED = 505;

    /**
     * Sets HTTP response code.
     *
     * @param int $code
     * @throws HttpException If HTTP headers have been sent
     * @throws InvalidArgumentException If code is unknown
     * @return IResponse
     */
    public function setCode(int $code): IResponse;

    /**
     * Sends a HTTP header and replaces a previous one.
     *
     * @param string $name
     * @param string $value
     * @return IResponse
     * @throws HttpException If HTTP headers have been sent
     */
    public function setHeader(string $name, string $value): IResponse;

    /**
     * Adds HTTP header.
     *
     * @param string $name
     * @param string $value
     * @return IResponse
     * @throws HttpException If HTTP headers have been sent
     */
    public function addHeader(string $name, string $value): IResponse;

    /**
     * Return the value of the HTTP header.
     *
     * @param string $header
     * @param mixed $default
     * @return mixed
     */
    public function getHeader(string $header, $default = NULL);

    /**
     * Sends a Content-type HTTP header.
     *
     * @param string $type
     * @param string|null $charset
     * @return IResponse
     * @throws HttpException If HTTP headers have been sent
     */
    public function setContentType(string $type, string $charset = NULL): IResponse;

    /**
     * Redirects to a new URL. Note: call exit() after it.
     *
     * @param string $url
     * @param int $code
     * @throws HttpException If HTTP headers have been sent
     */
    public function redirect(string $url, int $code = self::S302_FOUND);

    /**
     * Sets the number of seconds before a page cached on a browser expires.
     *
     * @param IDatetime $now
     * @param int $seconds
     * @return IResponse
     * @throws HttpException
     */
    public function setExpiration(IDatetime $now, int $seconds = 0): IResponse;

    /**
     * Checks if headers have been sent.
     *
     * @return bool
     */
    public function isSent(): bool;

    /**
     * Sends a cookie.
     *
     * @param string $name name of the cookie
     * @param string $value value
     * @param IDatetime $expire expiration time, value 0 means "until the browser is closed"
     * @param string|null $path
     * @param string|null $domain
     * @param bool|null $secure
     * @param bool|null $httpOnly
     * @return IResponse
     * @throws HttpException If HTTP headers have been sent
     */
    public function setCookie(
        string $name
      , string $value
      , IDatetime $expire
      , string $path = NULL
      , string $domain = NULL
      , bool $secure = NULL
      , bool $httpOnly = NULL
    ): IResponse;

    /**
     * Deletes a cookie.
     *
     * @param string $name name of the cookie.
     * @param string|null $path
     * @param string|null $domain
     * @param bool|null $secure
     * @return IResponse
     * @throws HttpException If HTTP headers have been sent
     */
    public function deleteCookie(string $name, string $path = NULL, string $domain = NULL, bool $secure = NULL): IResponse;

    /**
     * @param JsonSerializable|array $serializableData
     */
    public function outputJson($serializableData);

    public function outputXlsx($serializableData, string $filename = 'file');

    /**
     * @param string $data
     * @param string $contentType
     * @param int $contentLength
     * @param string|NULL $attachmentName
     * @param string $contentDisposition
     * @throws HttpException
     */
    public function outputBinary(
        string &$data
      , string $contentType
      , int $contentLength
      , string $attachmentName = NULL
      , string $contentDisposition = 'attachment'
    );
}
