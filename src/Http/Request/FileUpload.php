<?php

namespace MTi\Http\Request;


/**
 * Container for uploaded files
 * Inspired by Nette FileUpload
 */
class FileUpload
{
    /** @var string */
    private $name;
    /** @var string */
    private $type;
    /** @var string */
    private $contentType;
    /** @var int */
    private $size;
    /** @var string */
    private $tmpName;
    /** @var int */
    private $error;

    public static function getFromFile(string $file, string $type): self
    {
        $pathinfo = pathinfo($file);
        return new FileUpload(['name' => $pathinfo['basename'], 'type' => $type, 'tmp_name' => $file, 'error' => 0]);
    }

    public function __construct(array $value)
    {
        foreach (['name', 'type', 'tmp_name', 'error'] as $key) {
            if (!isset($value[$key]) || !is_scalar($value[$key])) {
                $this->error = UPLOAD_ERR_NO_FILE;
                return;
            }
        }
        $this->name = $value['name'];
        $this->type = $value['type'];
        $this->tmpName = $value['tmp_name'];
        $this->error = $value['error'] === 1;
    }

    /**
     * Returns the file name.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns the MIME content type of an uploaded file.
     */
    public function getContentType(): string
    {
        if ($this->isOk() && $this->contentType === NULL) {
            $this->contentType = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $this->tmpName);
        }
        return $this->contentType;
    }

    /**
     * Returns the size of an uploaded file.
     */
    public function getSize(): int
    {
        if ($this->isOk() && $this->size === NULL) {
            $this->size = filesize($this->getTemporaryFile());
        }
        return $this->size;
    }

    /**
     * Returns the path to an uploaded file.
     */
    public function getTemporaryFile(): string
    {
        return $this->tmpName;
    }

    /**
     * Returns the error code. {@link http://php.net/manual/en/features.file-upload.errors.php}
     */
    public function isError(): int
    {
        return $this->error;
    }

    /**
     * Is there any error?
     */
    public function isOk(): bool
    {
        return $this->error === UPLOAD_ERR_OK;
    }

    public function hasFile(): bool
    {
        return $this->error !== UPLOAD_ERR_NO_FILE;
    }

    /**
     * Get file contents.
     *
     * @return string|null
     */
    public function getContents(): ?string
    {
        // future implementation can try to work around safe_mode and open_basedir limitations
        return $this->isOk() ? file_get_contents($this->tmpName) : NULL;
    }
}
