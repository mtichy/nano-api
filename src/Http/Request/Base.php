<?php

namespace MTi\Http\Request;

use MTi\Http\IRequest;
use MTi\Input\BaseInputDataProvider;


abstract class Base
    extends BaseInputDataProvider
    implements IRequest
{

}