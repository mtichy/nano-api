<?php

namespace MTi\Http\Request;

use Darsyn\IP\AbstractIP;
use Darsyn\IP\Exception\InvalidIpAddressException;
use Darsyn\IP\Exception\IpException;
use Darsyn\IP\Exception\WrongVersionException;
use Darsyn\IP\Version\Multi;
use InvalidArgumentException;
use Logger;
use LogicException;
use MTi\Http\IRequest;
use Nette\Http\Url;


class HttpRequest
    extends Base
{
    public function __construct(Logger $logger)
    {
        $this->_logger = $logger;
        $logger->info($this->getMethod().': '.$this->getRequestUrl());
        $ct = $this->getContentType();
        if ($ct) {
            $logger->debug('CONTENT TYPE: '.$ct);
        }
        try {
            $logger->trace('REMOTE IP: ' . $this->getIP()->getDotAddress());
        }
        catch (WrongVersionException|IpException $e) {
            throw new LogicException();
        }
    }
    private $_data;
    private $_files;
    private $_logger;

    /**
     * @return bool
     */
    private function isJSON()
    {
        return $this->getContentType() == IRequest::CONTENT_TYPE_JSON;
    }

    /**
     * @return bool
     */
    private function isForm()
    {
        return in_array($this->getContentType(), [
            IRequest::CONTENT_TYPE_FORM,
            IRequest::CONTENT_TYPE_XFORM,
        ]);
    }

    /**
     * @return array
     */
    private function loadJsonInput()
    {
        $rawInput = file_get_contents("php://input");
        try {
            return \GuzzleHttp\json_decode($rawInput, TRUE);
        }
        catch (InvalidArgumentException $e) {
            $this->_logger->warn("Invalid JSON input.", $e);
            $this->_logger->debug($rawInput);
            return [];
        }
    }

    /**
     * @return array
     * @throws InvalidArgumentException
     */
    protected function getInputData(): array
    {
        if (is_null($this->_data)) {
            $method = $this->getMethod();
            if ($method == 'POST') {
                if ($this->isJSON()) {
                    $this->_data = $this->loadJsonInput();
                }
                elseif ($this->isForm()) {
                    $this->_data = $_REQUEST;
                }
                else {
                    $this->_data = [];
                }
            }
            elseif ($method == 'PUT') {
                if ($this->isJSON()) {
                    $this->_data = $this->loadJsonInput();
                }
                elseif ($this->isForm()) {
                    parse_str(file_get_contents("php://input"), $this->_data);
                    if (is_null($this->_data)) {
                        $this->_data = [];
                    }
                }
            }
            elseif (in_array($method, ['GET', 'DELETE'])) {
                $this->_data = $_GET;
            }
            else {
                $this->_data = [];
            }
            $this->_logger->debug(array_filter(
                $this->_data
              , function ($k)
                {
                    return $k[0] != '_';
                }
              , ARRAY_FILTER_USE_KEY
            ));
        }
        return $this->_data;
    }

    protected function getFiles(): array
    {
        if (is_null($this->_files)) {
            $this->_files = [];
            foreach ($_FILES as $key => $value) {
                $this->_files[$key] = [];
                if(is_array($value['name'])) {
                    $i = 0;
                    while (isset($value['name'][$i])) {
                        $this->_files[$key][] = new FileUpload(
                            [
                                'name' => $value['name'][$i],
                                'type' => $value['type'][$i],
                                'tmp_name' => $value['tmp_name'][$i],
                                'error' => $value['error'][$i],
                            ]
                        );
                        $i++;
                    }
                }
                else {
                    $this->_files[$key][] = new FileUpload(
                        [
                            'name' => $value['name'],
                            'type' => $value['type'],
                            'tmp_name' => $value['tmp_name'],
                            'error' => $value['error'],
                        ]
                    );
                }
            }
        }
        return $this->_files;
    }

    /**
     * {@inheritdoc}
     */
    public function getServerUrl(): Url
    {
        return new Url(sprintf('%s://%s'
          , isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http"
          , $_SERVER['HTTP_HOST']
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestUrl(): Url
    {
        return new Url(sprintf('%s://%s%s'
          , isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http"
          , $_SERVER['HTTP_HOST']
          , $_SERVER['REQUEST_URI']
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * {@inheritdoc}
     */
    public function getContentType(): ?string
    {
        if (isset($_SERVER["CONTENT_TYPE"])) {
            $parts = explode(';', $_SERVER["CONTENT_TYPE"]); // Nekdy obsahuje charset za ;
            return $parts[0];
        } else {
            return NULL;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIP(): AbstractIP
    {
        try {
            return Multi::factory($_SERVER['REMOTE_ADDR']);
        }
        catch (WrongVersionException|InvalidIpAddressException $e) {
            throw new LogicException("\$_SERVER['REMOTE_ADDR'] should contain IPv4 address.");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorizationHeader(): ?string
    {
        if (isset($_SERVER['Authorization'])) {
            return trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            return trim($_SERVER["HTTP_AUTHORIZATION"]);
        }
        elseif (function_exists('apache_request_headers')) {
            /** @noinspection PhpComposerExtensionStubsInspection */
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions
            // (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(
                array_map('ucwords', array_keys($requestHeaders))
              , array_values($requestHeaders)
            );
            if (isset($requestHeaders['Authorization'])) {
                return trim($requestHeaders['Authorization']);
            }
        }
        return NULL;
    }
}
