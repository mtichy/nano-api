<?php

namespace MTi\Http\Request;

use Darsyn\IP\AbstractIP;
use Darsyn\IP\Exception\InvalidIpAddressException;
use Darsyn\IP\Exception\WrongVersionException;
use Darsyn\IP\Version\Multi;
use Nette\Http\Url;


class TestRequest
    extends Base
{
    public function __construct(
        array $inputData
      , string $method = 'GET'
      , string $contentType = self::CONTENT_TYPE_JSON
      , string $ipv4addr = '8.8.8.8'
    )
    {
        $this->_data = $inputData;
        $this->_method = $method;
        $this->_ct = $contentType;
        $this->_ip = $ipv4addr;
    }
    private $_data;
    private $_method;
    private $_ct;
    private $_ip;

    protected function getInputData(): array
    {
        return $this->_data;
    }

    protected function getFiles(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod(): string
    {
        return $this->_method;
    }

    /**
     * {@inheritdoc}
     */
    public function getContentType(): ?string
    {
        return $this->_ct;
    }

    /**
     * {@inheritdoc}
     */
    public function getServerUrl(): Url
    {
        return new Url();
    }

    public function getRequestUrl(): Url
    {
        return new Url();
    }

    /**
     * {@inheritdoc}
     * @throws InvalidIpAddressException
     * @throws WrongVersionException
     */
    public function getIP(): AbstractIP
    {
        return Multi::factory($this->_ip);
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorizationHeader(): ?string
    {
        return NULL;
    }
}
