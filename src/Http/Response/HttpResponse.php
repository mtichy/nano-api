<?php

namespace MTi\Http\Response;

use ArrayIterator;
use DateInterval;
use DateTimeZone;
use Exception;
use InvalidArgumentException;
use JsonSerializable;
use Logger;
use LoggerLevel;
use LogicException;
use MTi\DateTime\ExtendedDatetime;
use MTi\Http\HttpException;
use MTi\Http\IRequest;
use MTi\Http\IResponse;
use MTi\I18n\Util\CzeStringHelper;
use MTi\IDatetime;
use MTi\Type\UnicodeString;
use MTi\Util\MsExcel;
use MTi\Util\Json;
use Nette\Utils\JsonException;


final class HttpResponse
    implements IResponse
{
    /**
     * Returns HTTP valid date format.
     *
     * @param ExtendedDatetime $dt
     * @return string
     */
    public static function date(ExtendedDatetime $dt)
    {
        $dt->setTimezone(new DateTimeZone('GMT'));
        return $dt->format('D, d M Y H:i:s \G\M\T');
    }


    public function __construct(Logger $logger, bool $developmentMode)
    {
        $this->_logger = $logger;
        $this->_dvl = $developmentMode;
    }
    private $_logger;
    private $_dvl;

    /** @var bool  Send invisible garbage for IE 6? */
	private static $fixIE = true;
	/** @var string The domain in which the cookie will be available */
	public $cookieDomain = '';
	/** @var string The path in which the cookie will be available */
	public $cookiePath = '/';
	/** @var string Whether the cookie is available only through HTTPS */
	public $cookieSecure = '';
	/** @var string Whether the cookie is hidden from client-side */
	public $cookieHttpOnly = '';
	/** @var int HTTP response code */
	private $code = self::S200_OK;

    /**
     * {@inheritdoc}
     */
	public function setCode(int $code): IResponse
	{
		static $allowed = [
			200=>1, 201=>1, 202=>1, 203=>1, 204=>1, 205=>1, 206=>1,
			300=>1, 301=>1, 302=>1, 303=>1, 304=>1, 307=>1,
			400=>1, 401=>1, 403=>1, 404=>1, 405=>1, 406=>1, 408=>1, 410=>1, 412=>1, 415=>1, 416=>1,
			500=>1, 501=>1, 503=>1, 505=>1
		];
		if (!isset($allowed[$code])) {
			throw new InvalidArgumentException("Bad HTTP response '$code'.");
		}
		elseif (headers_sent($file, $line)) {
			throw new HttpException(
                "Cannot set HTTP code after HTTP headers have been sent" . (
                    $file ? " (output started at $file:$line)." : "."
                )
            );
		}
		else {
			$this->code = $code;
			$protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1';
			header($protocol . ' ' . $code, true, $code);
		}
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setHeader(string $name, string $value = NULL): IResponse
	{
		if (headers_sent($file, $line)) {
			throw new HttpException(
                "Cannot send header after HTTP headers have been sent" . (
                    $file ? " (output started at $file:$line)." : "."
                )
            );
		}
		if ($value === null && function_exists('header_remove')) {
			header_remove($name);
		}
		else {
			header($name . ': ' . $value, true, $this->code);
		}
		return $this;
	}

    /**
     * {@inheritdoc}
     */
	public function addHeader(string $name, string $value): IResponse
	{
		if (headers_sent($file, $line)) {
			throw new HttpException(
                "Cannot send header after HTTP headers have been sent" . (
                    $file ? " (output started at $file:$line)." : "."
                )
            );
		}
		header($name . ': ' . $value, false, $this->code);
		return $this;
	}

    /**
     * {@inheritdoc}
     */
    public function getHeader(string $header, $default = NULL)
    {
        $header .= ':';
        $len = strlen($header);
        foreach (headers_list() as $item) {
            if (strncasecmp($item, $header, $len) === 0) {
                return ltrim(substr($item, $len));
            }
        }
        return $default;
    }

	/**
	 * {@inheritdoc}
	 */
	public function setContentType(string $type, string $charset = null): IResponse
	{
		$this->setHeader(
            'Content-Type'
          , $type . ($charset ? '; charset=' . $charset : '')
        );
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function redirect(string $url, int $code = self::S302_FOUND)
	{
		if (isset($_SERVER['SERVER_SOFTWARE'])
            && preg_match('#^Microsoft-IIS/[1-5]#', $_SERVER['SERVER_SOFTWARE'])
			&& $this->getHeader('Set-Cookie') !== null
		) {
			$this->setHeader('Refresh', "0;url=$url");
			return;
		}
		$this->setCode($code);
		$this->setHeader('Location', $url);
		echo "<h1>Redirect</h1>\n\n<p><a href=\""
            . htmlspecialchars($url)
            . "\">Please click here to continue</a>.</p>"
        ;
	}

    /**
     * {@inheritdoc}
     */
	public function setExpiration(IDatetime $now, int $seconds = 0): IResponse
	{
		if ($seconds == 0) { // no cache
			$this->setHeader('Cache-Control', 's-maxage=0, max-age=0, must-revalidate');
			$this->setHeader('Expires', 'Mon, 23 Jan 1978 10:00:00 GMT');
			return $this;
		}
		$this->setHeader('Cache-Control', 'max-age=' . $seconds);
        try {
            $expiryInterval = new DateInterval(sprintf('PT%dS', $seconds));
        }
        catch (Exception $e) {
            throw new LogicException();
        }
        $this->setHeader(
		    'Expires'
          , $now->cloneMe()->add($expiryInterval)
        );
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isSent(): bool
	{
		return headers_sent();
	}

	public function __destruct()
	{
		if (
		    self::$fixIE
            && isset($_SERVER['HTTP_USER_AGENT'])
            && strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE ') !== false
			&& in_array(
			    $this->code
              , [
                    IResponse::S400_BAD_REQUEST,
                    IResponse::S403_FORBIDDEN,
                    IResponse::S404_NOT_FOUND,
                    IResponse::S405_METHOD_NOT_ALLOWED,
                    IResponse::S406_NOT_ACCEPTABLE,
                    IResponse::S408_REQUEST_TIMEOUT,
                    IResponse::S409_CONFLICT,
                    IResponse::S410_GONE,
                    IResponse::S500_INTERNAL_SERVER_ERROR,
                    IResponse::S501_NOT_IMPLEMENTED,
                    IResponse::S505_HTTP_VERSION_NOT_SUPPORTED,
                ]
              , TRUE
            )
			&& $this->getHeader('Content-Type', 'text/html') === 'text/html'
		) {
			echo UnicodeString::random(2000, " \t\r\n"); // sends invisible garbage for IE
			self::$fixIE = false;
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function setCookie(
        string $name
      , string $value
      , IDatetime $expire
      , string $path = null
      , string $domain = null
      , bool $secure = null
      , bool $httpOnly = null
    ): IResponse
	{
		if (headers_sent($file, $line)) {
			throw new HttpException(
                "Cannot set cookie after HTTP headers have been sent" . ($file ? " (output started at $file:$line)." : ".")
            );
		}
		setcookie(
			$name
          ,	$value
          ,	$expire->getTimestamp()
          ,	$path === null ? $this->cookiePath : $path
          ,	$domain === null ? $this->cookieDomain : $domain
          ,	$secure === null ? $this->cookieSecure : $secure
          ,	$httpOnly === null ? $this->cookieHttpOnly : $httpOnly
		);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function deleteCookie(string $name, string $path = NULL, string $domain = NULL, bool $secure = NULL): IResponse
	{
		if (headers_sent($file, $line)) {
			throw new HttpException(
                "Cannot delete cookie after HTTP headers have been sent" . ($file ? " (output started at $file:$line)." : ".")
            );
		}
		setcookie(
			$name
          ,	false
          ,	254400000
          ,	$path === null ? $this->cookiePath : $path
          ,	$domain === null ? $this->cookieDomain : $domain
          ,	$secure === null ? $this->cookieSecure : $secure
          ,	true // doesn't matter with delete
		);
		return $this;
	}

    /**
     * {@inheritdoc}
     */
	public function outputJson($serializableData)
    {
        if (is_array($serializableData)) {
            try {
                Json::ensureJson($serializableData);
            }
            catch (JsonException $e) {
                $this->_logger->warn("Invalid data for JSON:\n".var_export($serializableData, TRUE));
                throw new LogicException("Faulty JSON serialization!", -1, $e);
            }
        }
        elseif (!($serializableData instanceof JsonSerializable)) {
            throw new InvalidArgumentException('Data for output are not serializable');
        }
        try {
            $this->setContentType(IRequest::CONTENT_TYPE_JSON);
        }
        catch (HttpException $e) {
            throw new LogicException();
        }
        $this->_logger->info('RETURN CODE: '.$this->code);
        if ($this->_dvl) {
            $o = json_encode($serializableData, JSON_PRETTY_PRINT);
        } else {
            $o = json_encode($serializableData);
        }
        $this->_logger->log(
            $this->code === self::S200_OK ? LoggerLevel::getLevelTrace() : LoggerLevel::getLevelDebug()
          , $o
        );
        echo $o;
        exit(0);
    }

    /**
     * @param $serializableData
     * @param string $filename
     */
    public function outputXlsx($serializableData, string $filename = 'file')
    {
        $this->_logger->info('RETURN CODE: ' . $this->code);
        try {
            if ($serializableData instanceof ArrayIterator) {
                $serializableData = $serializableData->getArrayCopy();
            }
            if ($serializableData instanceof JsonSerializable) {
                $serializableData = $serializableData->jsonSerialize();
            }
            $writer = MsExcel::arrayToXls($serializableData);
            try {
                $this
                    ->setContentType('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    ->setHeader('Content-Disposition', 'attachment;filename="'.$filename.'.xlsx')
                    ->setHeader('Cache-Control', 'max-age=0')
                ;
            }
            catch (HttpException $e) {
                throw new LogicException();
            }
            $writer->save('php://output');
        }
        catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            throw new LogicException("Faulty XLSX serialization!", -1, $e);
        }
        exit(0);
    }

    /**
     * {@inheritdoc}
     */
    public function outputBinary(
        string &$data
      , string $contentType
      , int $contentLength
      , string $attachmentName = NULL
      , string $contentDisposition = 'attachment'
    )
    {
        $enc = NULL;
        if (substr($contentType, 0, 5 ) === "text/") {
            // TODO: support for other languages / charsets
            $enc = CzeStringHelper::detectEncoding($data);
        }
        $this
            ->setContentType($contentType, $enc)
            ->setHeader('Content-Length', strval($contentLength))
        ;
        if (!is_null($attachmentName)) {
            $this->setHeader(
                'Content-Disposition'
              , $contentDisposition.'; filename=' . $attachmentName
            );
        }
        echo $data;
        exit(0);
    }
}
