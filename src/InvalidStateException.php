<?php

namespace MTi;

use RuntimeException;


class InvalidStateException
    extends RuntimeException
{
}
