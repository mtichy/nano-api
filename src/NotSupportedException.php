<?php

namespace MTi;

use LogicException;


class NotSupportedException
    extends LogicException
{
}
