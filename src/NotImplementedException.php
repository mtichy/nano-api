<?php

namespace MTi;

use LogicException;


class NotImplementedException
    extends LogicException
{
}
