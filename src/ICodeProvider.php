<?php

namespace MTi;


interface ICodeProvider
{
    public function getCode();
}
