<?php

namespace MTi;

use RuntimeException;


class IOException
    extends RuntimeException
{
}
