<?php

namespace MTi\ORM;

use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use JsonSerializable;
use LogicException;


class Timestamp
    extends DateTime
    implements JsonSerializable
{
    public static function currentTimestamp(DateTimeZone $tz = NULL)
    {
        preg_match('/^0\.(\d{6})\d{2} (\d+)$/', microtime(), $m);
        $dt = DateTime::createFromFormat('U.u', $m[2].'.'.$m[1], new DateTimeZone('UTC'));
        try {
            $interval = new DateInterval(sprintf('PT%dS', $tz->getOffset($dt)));
            $dt->add($interval);
            return new static($dt->format('Y-m-d H:i:s.u'), $tz);
        }
        catch (Exception $e) {
            throw new LogicException();
        }
    }

    public function __toString()
    {
        return $this->format('Y-m-d H:i:s.u');
    }

    public static function fromDateTime(DateTime $dateTime)
    {
        try {
            return new static($dateTime->format('Y-m-d H:i:s.u'));
        }
        catch (Exception $e) {
            throw new LogicException();
        }
    }

    public function jsonSerialize()
    {
        return $this->__toString();
    }
}
