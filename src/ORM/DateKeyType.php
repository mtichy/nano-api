<?php

namespace MTi\ORM;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateType;
use MTi\DateTime\DateTime;


class DateKeyType
    extends DateType
{
    /**
     * @param $value
     * @param AbstractPlatform $platform
     * @return bool|DateTime|\DateTime|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = parent::convertToPHPValue($value, $platform);
        if ($value !== NULL) {
            $value = DateTime::fromDateTime($value);
        }
        return $value;
    }

    public function getName()
    {
        return 'datekey';
    }
}
