<?php

namespace MTi\ORM;

use DateTime;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeType;


class TimestampType
    extends DateTimeType
{
    private $format = 'Y-m-d H:i:s.u';

    /**
     * @param $value
     * @param AbstractPlatform $platform
     * @return Timestamp|null
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === NULL) {
            return NULL;
        }
        if ($value instanceof DateTime) {
            return Timestamp::fromDateTime($value);
        }
        $val = Timestamp::createFromFormat($this->format, $value);
        if (!$val) {
            $val = date_create($value);
        }
        if (!$val) {
            throw ConversionException::conversionFailedFormat(
                $value
              , $this->getName()
              , $this->format
            );
        }
        return Timestamp::fromDateTime($val);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var DateTime $value */
        return ($value !== NULL)
            ? $value->format($this->format)
            : NULL
        ;
    }

    public function getName()
    {
        return 'timestamp';
    }
}
