<?php

namespace MTi;

use DateTimeZone;
use Doctrine\ORM\EntityManager;
use Logger;
use MTi\Http\IRequest;
use MTi\Http\IResponse;
use MTi\Security\AuthenticationException;
use MTi\Security\IAuthenticator;
use MTi\Security\IIdentity;
use MTi\Util\EnvWrapper;
use Nette\Caching\Cache;


interface IEnv
{
    /**
     * @param string $section
     * @return IConfig
     */
    public function config(string $section = NULL): IConfig;

    /**
     * @return string
     */
    public function cacheDir(): string;

    /**
     * @return string
     */
    public function logDir(): string;

    /**
     * @return string
     */
    public function proxyDir(): string;

    /**
     * @return IRequest
     */
    public function request(): IRequest;

    /**
     * @return IResponse
     */
    public function response(): IResponse;

    /**
     * @param string|null $name
     * @return Logger
     */
    public function logger(string $name = NULL): Logger;

    /**
     * @return EntityManager
     */
    public function em(): EntityManager;

    /**
     * @return IAuthenticator
     */
    public function authenticator(): IAuthenticator;

    /**
     * @return IIdentity
     * @throws AuthenticationException Error during authentication process / session expired / explicit login needed
     */
    public function user(): IIdentity;

    /**
     * @param string $namespace
     * @return Cache
     */
    public function cache(string $namespace = ''): Cache;

    /**
     * Returns environment identifier.
     *
     * @return string
     */
    public function stage(): string;

    /**
     * @return bool
     */
    public function isProduction(): bool;

    /**
     * @return bool
     */
    public function isDevelopment(): bool;

    /**
     * Return DTZ object for given TZ name, or default zone from configuration.
     *
     * @param string|null $tzName Timezone identifier accepted by \DateTimeZone
     * @return DateTimeZone
     * @throws UnsupportedTimeZoneException
     */
    public function getDateTimeZone(string $tzName = NULL): DateTimeZone;

    /**
     * @return IDate
     */
    public function currentDate(): IDate;

    /**
     * @param string|null $tzName
     * @return IDatetime
     * @throws UnsupportedTimeZoneException
     */
    public function currentDatetime(string $tzName = NULL): IDatetime;

    /**
     * Returns classname of specific Money descendant used by application.
     *
     * @return string
     */
    public function moneyClassName(): string;

    /**
     * Callback wrapper factory.
     *
     * @param string $name
     * @return EnvWrapper
     * @throws NotImplementedException
     */
    public function getWrapperByName(string $name): EnvWrapper;

    /**
     * If true, it means the system is down and should not permit any operations.
     *
     * @return bool
     */
    public function isInMaintenance(): bool;
}
