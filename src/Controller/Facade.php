<?php

namespace MTi\Controller;

use Doctrine\ORM\EntityRepository;
use InvalidArgumentException;
use MTi\IEnv;
use MTi\Input\CollectorFormInput;
use MTi\Input\FormInput;
use MTi\Input\IInputDataProvider;


abstract class Facade
    extends Front
{
    public function __construct(IEnv $ue)
    {
        parent::__construct($ue);
        $this->_ue = $ue;
    }
    private $_ue;

    protected function getRepository(string $className): EntityRepository
    {
        return $this->_ue->em()->getRepository($className);
    }

    protected function createCollectorDPInput(
        string $inputClassName
      , IInputDataProvider $dataProvider
      , bool $update
      , array $defaults = []
      , string $varnamePrefix = NULL
    ): CollectorFormInput
    {
        if (!is_subclass_of($inputClassName, CollectorFormInput::class)) {
            throw new InvalidArgumentException();
        }
        return new $inputClassName(
            $dataProvider
          , $this->_ue->em()
          , $this->_ue->getDateTimeZone()
          , $this->_ue->moneyClassName()
          , $update
          , $defaults
          , $varnamePrefix
        );
    }

    protected function createCollectorFormInput(
        string $inputClassName
      , bool $update
      , array $defaults = []
      , string $varnamePrefix = NULL
    ): CollectorFormInput
    {
        return $this->createCollectorDPInput(
            $inputClassName
          , $this->_ue->request()
          , $update
          , $defaults
          , $varnamePrefix
        );
    }

    protected function createFormDPInput(string $inputClassName, IInputDataProvider $dataProvider, bool $update): FormInput
    {
        if (
            !is_subclass_of($inputClassName, FormInput::class)
            || is_subclass_of($inputClassName, CollectorFormInput::class)
        ) {
            throw new InvalidArgumentException();
        }
        return new $inputClassName($dataProvider, $this->_ue->moneyClassName(), $update);
    }

    protected function createFormInput(string $inputClassName, bool $update): FormInput
    {
        return $this->createFormDPInput($inputClassName, $this->_ue->request(), $update);
    }
}
