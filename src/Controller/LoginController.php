<?php

namespace MTi\Controller;

use Logger;
use LogicException;
use MTi\IEnv;
use MTi\Input\EmptyParameterException;
use MTi\Input\InvalidParameterException;
use MTi\Security\AuthenticationException;


class LoginController
    extends BaseController
{
    public function __construct(IEnv $ue)
    {
        parent::__construct($ue);
        $this->_auth = $ue->authenticator();
    }
    private $_auth;

    protected function logger(): Logger
    {
        return $this->getLoggerByName('void');
    }

    public function login()
    {
        try {
            $login = $this->input()->getString('username', '');
            $password = $this->input()->getString('password', '');
        }
        catch (EmptyParameterException|InvalidParameterException $e) {
            throw new LogicException();
        }
        if (!($login && $password)) {
            throw new AuthenticationException('Missing credentials', AuthenticationException::WRONG_CREDS);
        }
        $ret = $this->_auth->doExplicitLogin($login, $password);
        if (!empty($ret)) {
            $this->outputJson($ret);
        }
    }
}
