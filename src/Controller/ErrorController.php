<?php

namespace MTi\Controller;

use InvalidArgumentException;
use Logger;
use MTi\Http\HttpException;


class ErrorController
    extends BaseController
{
    protected function logger(): Logger
    {
        return $this->getLoggerByName('void');
    }

    public function quit(int $httpCode, string $errorCode, string $message = NULL, string $file = NULL, int $line = NULL)
    {
        try {
            $this->response()->setCode($httpCode);
        }
        catch (HttpException $e) {
            throw new InvalidArgumentException();
        }
        $r = [
            'success' => FALSE,
            'error' => $errorCode,
        ];
        if ($message && !$this->isProductionEnvironment()) {
            $r['message'] = $message;
        }
        if ($this->isDevelopmentEnvironment()) {
            if ($file) {
                $r['file'] = $file;
            }
            if ($line > 0) {
                $r['line'] = $line;
            }
        }
        $this->outputJson($r);
    }
}
