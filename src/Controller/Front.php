<?php

namespace MTi\Controller;

use Logger;
use MTi\IConfig;
use MTi\IDate;
use MTi\IDatetime;
use MTi\IEnv;
use MTi\Security\IIdentity;


abstract class Front
{
    public function __construct(IEnv $ue)
    {
        $this->_ue = $ue;;
    }
    private $_ue;

    final protected function config(string $section): IConfig
    {
        return $this->_ue->config($section);
    }

    /**
     * @return IDate
     */
    final protected function today(): IDate
    {
        return $this->_ue->currentDate();
    }

    /**
     * @return IDatetime
     */
    final protected function now(): IDatetime
    {
        return $this->_ue->currentDatetime();
    }

    final protected function getLoggerByName(string $name): Logger
    {
        return $this->_ue->logger($name);
    }

    /**
     * @return Logger
     */
    abstract protected function logger(): Logger;

    /**
     * @return IIdentity
     */
    final protected function user(): IIdentity
    {
        return $this->_ue->user();
    }

    protected function isDevelopmentEnvironment(): bool
    {
        return $this->_ue->isDevelopment();
    }

    protected function isProductionEnvironment(): bool
    {
        return $this->_ue->isProduction();
    }
}
