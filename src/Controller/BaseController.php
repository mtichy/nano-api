<?php

namespace MTi\Controller;

use BadMethodCallException;
use MTi\Entity\ITableKeyProvider;
use MTi\Http\HttpException;
use MTi\Http\IRequest;
use MTi\Http\IResponse;
use MTi\IEnv;
use MTi\Input\EmptyParameterException;
use MTi\Input\InputValueParser;
use MTi\Input\InvalidParameterException;


abstract class BaseController
    extends Front
{
    protected const FORMAT_FIELD_NAME = '_format';
    protected const DEFAULT_OUTPUT_FORMAT = 'JSON';

    public function __construct(IEnv $ue)
    {
        parent::__construct($ue);
        $this->_ue = $ue;;
    }
    private $_ue;

    /**
     * @param string $action
     */
    public function beforeDispatch(string $action)
    {
    }

    /**
     * @return IRequest
     */
    protected function request(): IRequest
    {
        return $this->_ue->request();
    }

    /**
     * @return InputValueParser
     */
    protected function input(): InputValueParser
    {
        if (!$this->_vp) {
            $this->_vp = new InputValueParser($this->request(), $this->_ue->moneyClassName());
        }
        return $this->_vp;
    }
    private $_vp;

    /**
     * @return IResponse
     */
    protected function response(): IResponse
    {
        return $this->_ue->response();
    }

    protected function checkRequestMethod(string $method)
    {
        if ($this->request()->getMethod() != strtoupper($method)) {
            throw new BadMethodCallException();
        }
    }

    /**
     * @param mixed $serializableData
     * @throws InvalidParameterException
     * @throws EmptyParameterException
     */
    protected function outputData($serializableData)
    {
        $format = $this->input()->getString(self::FORMAT_FIELD_NAME, self::DEFAULT_OUTPUT_FORMAT);
        if (strtoupper($format) === 'JSON') {
            $this->outputJson($serializableData);
        }
        elseif (strtoupper($format) === 'XLSX') {
            $this->outputXlsx($serializableData);
        }
        else {
            throw new InvalidParameterException(self::FORMAT_FIELD_NAME, InvalidParameterException::WRONG);
        }
    }

    /**
     * @param mixed $serializableData
     */
    protected function outputJson($serializableData)
    {
        $this->response()->outputJson($serializableData);
    }

    protected function removeTableKey(&$serializableData)
    {
        if (is_array($serializableData)) {
            foreach ($serializableData as $key => &$val) {
                if ($key === ITableKeyProvider::ARRAY_INDEX_KEY) {
                    unset($serializableData[$key]);
                }
                else {
                    $this->removeTableKey($val);
                }
            }
        }
    }

    /**
     * @param mixed $serializableData
     */
    protected function outputXlsx($serializableData)
    {
        $this->removeTableKey($serializableData);
        $this->response()->outputXlsx($serializableData);
    }

    /**
     * @param string $data
     * @param string $contentType
     * @param int $contentLength
     * @param string|NULL $attachmentName
     * @param string $contentDisposition
     * @throws HttpException
     */
    protected function outputBinary(
        string &$data
      , string $contentType
      , int $contentLength
      , string $attachmentName = NULL
      , string $contentDisposition = 'attachment'
    )
    {
        $this->response()->outputBinary($data, $contentType, $contentLength, $attachmentName, $contentDisposition);
    }

    protected function outputSuccess()
    {
        $this->outputJson([
            'success' => TRUE,
            'error' => NULL,
        ]);
    }
}
