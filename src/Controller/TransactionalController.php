<?php

namespace MTi\Controller;

use BadMethodCallException;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\MappingException;
use LogicException;
use MTi\Entity\BaseEntity;
use MTi\IEnv;
use MTi\Input\InvalidParameterException;


abstract class TransactionalController
    extends BaseController
{
    public function __construct(IEnv $ue)
    {
        parent::__construct($ue);
        $this->_em = $ue->em();
    }
    private $_em;

    public function __destruct()
    {
        $em = $this->_em;
        if ($em && $em->getConnection()->isConnected() && $em->getConnection()->isTransactionActive()) {
            try {
                $em->rollback();
                $em->getConnection()->close();
            }
                /** @noinspection PhpRedundantCatchClauseInspection */
            catch (ConnectionException $e) {
                ; // Nevermind. Controller is ending.
            }
        }
    }

    /**
     * @param string $action
     * @throws BadMethodCallException If HTTP method is unsupported / wrong.
     */
    public function beforeDispatch(string $action)
    {
        $httpMethod = $this->request()->getMethod();
        if (in_array($httpMethod, ['POST', 'PUT', 'DELETE'])) {
            if ($this->isTransactionsEnabled()) {
                $this->beginTransaction();
            }
        }
        elseif ($httpMethod != 'GET') {
            throw new BadMethodCallException();
        }
    }

    protected function isTransactionsEnabled()
    {
        return TRUE;
    }

    protected function beginTransaction(): void
    {
        $this->_em->beginTransaction();
    }

    protected function commitTransaction(): void
    {
        try {
            $this->_em->commit();
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (ConnectionException $e) {/* nevermind */}
    }

    protected function getRepository(string $className): EntityRepository
    {
        return $this->_em->getRepository($className);
    }

    /**
     * @param string $entityName
     * @return Type|string
     */
    private function getSinglePrimaryKeyDataType(string $entityName)
    {
        try {
            $metadata = $this->_em->getClassMetadata($entityName);
            $pkFieldName = $metadata->getSingleIdentifierFieldName();
            $datatype = $metadata->getTypeOfField($pkFieldName);
            if (NULL === $datatype) {
                throw new LogicException();
            }
            return $datatype;
        }
        catch (MappingException $e) {
            throw new LogicException('Cannot use getEntityFromInputById for entity '.$entityName);
        }
    }

    /**
     * @param string $inputName
     * @param Type|string $datatype
     * @param bool $required
     * @return string|int|null
     * @throws InvalidParameterException
     */
    private function getInputValueByDatatype(string $inputName, $datatype, bool $required = TRUE)
    {
        $found = $this->request()->hasData($inputName);
        if ($found) {
            if ('integer' === $datatype || 'smallint' === $datatype) {
                return $this->input()->getInt($inputName);
            }
            else {
                return $this->input()->getString($inputName);
            }
        }
        elseif ($required) {
            throw new InvalidParameterException($inputName, InvalidParameterException::WRONG);
        }
        else {
            return NULL;
        }
    }

    /**
     * @param string $entityName
     * @param string $inputName
     * @param bool $required
     * @return BaseEntity|null
     * @throws InvalidParameterException
     */
    protected function getEntityFromInputById(string $entityName, string $inputName, bool $required = TRUE): ?BaseEntity
    {
        $id = $this->getInputValueByDatatype(
            $inputName
          , $this->getSinglePrimaryKeyDataType($entityName)
          , $required
        );
        if ($id === '') {
            if ($required) {
                throw new InvalidParameterException($inputName, InvalidParameterException::WRONG);
            }
            return NULL;
        }
        /** @var BaseEntity $entity */
        $entity = $this->getRepository($entityName)->find($id);
        if (!$entity) {
            throw new InvalidParameterException($inputName, InvalidParameterException::WRONG);
        }
        return $entity;
    }

    /**
     * @param string $entityName
     * @param string $inputName
     * @param bool $allowEmpty
     * @return BaseEntity[]
     * @throws InvalidParameterException
     */
    protected function getEntityListFromInputByArray(string $entityName, string $inputName, $allowEmpty = TRUE): array
    {
        $array = $this->input()->getArray($inputName, $allowEmpty);
        $entityList = [];
        $datatype = $this->getSinglePrimaryKeyDataType($entityName);
        foreach ($array as $key => $id) {
            $this->getInputValueByDatatype($inputName . '/' . $key, $datatype);
            /** @var BaseEntity $entity */
            $entity = $this->getRepository($entityName)->find($id);
            if (!$entity) {
                throw new InvalidParameterException($inputName, InvalidParameterException::WRONG);
            }
            $entityList[] = $entity;
        }
        return $entityList;
    }
}
