<?php

namespace MTi\Input;


class EmptyParameterException
    extends InvalidParameterException
{
    public function __construct($parameters)
    {
        parent::__construct($parameters, self::WRONG);
    }
}
