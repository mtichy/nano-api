<?php

namespace MTi\Input;


interface IInputDataProvider
{
    /**
     * @param string $varname
     * @return mixed
     * @throws InvalidParameterException
     */
    public function getUnsecureData(string $varname);

    /**
     * @param string $varname
     * @param mixed|null $default
     * @param bool $trim
     * @return mixed
     * @throws InvalidParameterException
     */
    public function getSecureData(string $varname, $default = NULL, bool $trim = TRUE);

    /**
     * @param string $varname
     * @param array $default
     * @return array
     * @throws InvalidParameterException
     */
    public function getFile(string $varname, array $default = null): array;

    /**
     * @param string $varname
     * @return bool
     */
    public function hasData(string $varname): bool;
}
