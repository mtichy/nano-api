<?php

namespace MTi\Input;

use Closure;
use Doctrine\ORM\EntityManager;


class InputValueProcessor
{
    /** @var EntityManager */
    private $_em;

    /** @var string */
    private $_cn;

    /** @var array */
    private $_logic = [];

    public function setEntityCreator(EntityManager $entityManager, string $className): InputValueProcessor
    {
        $this->_em = $entityManager;
        $this->_cn = $className;
        return $this;
    }

    /**
     * Lambda function should accept 2 parameters - item name and it's value, it should return [modified] value, or throw InvalidParameterException.
     *
     * @param Closure $logic
     * @param bool $allowNull
     * @return InputValueProcessor
     */
    public function addLogic(Closure $logic, bool $allowNull = FALSE): InputValueProcessor
    {
        array_push($this->_logic, [
            'func' => $logic,
            'null' => $allowNull,
        ]);
        return $this;
    }

    /**
     * @param string $varname
     * @param mixed $value
     * @return mixed
     * @throws InvalidParameterException
     */
    public function process(string $varname, $value)
    {
        if ($this->_em && $this->_cn && (!empty($value) || $value === 0)) {
            $value = $this->_em->getRepository($this->_cn)->find($value);
            if (is_null($value)) {
                throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
            }
        }
        foreach ($this->_logic as $logic) {
            if (is_null($value) && !$logic['null']) {
                continue;
            }
            /** @var Closure $func */
            $func = $logic['func'];
            $value = call_user_func_array($func, [$varname, $value]);
        }
        return $value;
    }
}
