<?php

namespace MTi\Input;

use InvalidArgumentException;


abstract class BaseInputDataProvider
    implements IInputDataProvider
{
    abstract protected function getInputData(): array;

    /**
     * {@inheritdoc}
     */
    public function getUnsecureData(string $varname)
    {
        if (!$this->hasData($varname)) {
            return NULL;
        }
        $data = $this->getInputData();
        if (strpos($varname, '/') === FALSE) {
            $key = $varname;
        }
        else {
            $path = preg_split('#/#', $varname);
            for ($i = 0; $i < count($path) - 1; $i++) {
                if (!array_key_exists($path[$i], $data)) {
                    throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
                }
                $data = $data[$path[$i]];
            }
            $key = $path[count($path)-1];
        }
        try {
            return $data[$key];
        }
        catch (InvalidArgumentException $e) {
            throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
        }
    }

    private function stripTags($var)
    {
        if (!is_array($var)) {
            return strip_tags($var);
        }
        foreach ($var as $k => $v) {
            $var[$k] = $this->stripTags($v);
        }
        return $var;
    }

    /**
     * {@inheritdoc}
     */
    public function getSecureData(string $varname, $default = null, bool $trim = true)
    {
        $var = $this->getUnsecureData($varname);
        if (!is_null($var)) {
            if ($trim && is_string($var)) {
                $var = trim($var);
            }
            return $this->stripTags($var);
        }
        return $default;
    }

    /**
     * {@inheritdoc}
     */
    public function hasData(string $varname): bool
    {
        if (strpos($varname, '/') === FALSE) {
            return array_key_exists($varname, $this->getInputData());
        }
        $path = preg_split('#/#', $varname);
        $data = $this->getInputData();
        for ($i = 0; $i < count($path) - 1; $i++) {
            if (!array_key_exists($path[$i], $data)) {
                return FALSE;
            }
            $data = $data[$path[$i]];
        }
        return array_key_exists($path[count($path)-1], $data);

    }

    abstract protected function getFiles(): array;

    /**
     * {@inheritdoc}
     */
    public function getFile(string $varname, array $default = NULL): array
    {
        $files = $this->getFiles();
        if (isset($files[$varname])) {
            return $files[$varname];
        }
        else {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
        }
        return $default;
    }
}