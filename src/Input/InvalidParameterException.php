<?php

namespace MTi\Input;

use RuntimeException;


class InvalidParameterException
    extends RuntimeException
{
    const MISSING = 'MISSING';
    const WRONG = 'WRONG';
    const MISSINGORWRONG = 'MISSING_OR_WRONG';
    const IGNORED = 'IGNORED';
    const IMMUTABLE = 'IMMUTABLE';

    private static $codes = [
        self::MISSING => -2,
        self::WRONG => -3,
        self::MISSINGORWRONG => -1,
        self::IGNORED => -4,
        self::IMMUTABLE => -5,
    ];

    /**
     * @var string|array
     */
    private $parameters;

    /**
     * @var string
     */
    private $type;

    /**
     * InvalidParameterException constructor.
     * @param string|array $parameters
     * @param string $type
     */
    public function __construct($parameters, string $type = self::MISSINGORWRONG)
    {
        $this->parameters = $parameters;
        $this->type = $type;
        parent::__construct(
            ucfirst(strtolower($type)) . ' parameter ' . (is_array($parameters)
                ? implode(',', $parameters)
                : $parameters
            )
          , array_key_exists($type, self::$codes) ? self::$codes[$type] : 0
        );
    }
}
