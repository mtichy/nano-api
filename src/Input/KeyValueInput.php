<?php

namespace MTi\Input;


abstract class KeyValueInput
{
    /**
     * @param string $item
     * @return bool
     */
    abstract protected function hasKey(string $item): bool;

    /**
     * @param string $item
     * @return bool
     * @throws InvalidParameterException
     */
    abstract protected function hasValue(string $item): bool;

    /**
     * @param string $item
     * @param mixed $default
     * @return string
     * @throws InvalidParameterException
     */
    abstract protected function getData(string $item, $default = null);
}
