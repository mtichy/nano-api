<?php

namespace MTi\Input;


abstract class JSONInput
    extends KeyValueInput
{
    public function __construct(string $json)
    {
        $this->_data = json_decode($json,TRUE);
    }
    private $_data;

    /**
     * {@inheritdoc}
     */
    protected function hasKey(string $item): bool
    {
        return array_key_exists($item, $this->_data);
    }

    /**
     * {@inheritdoc}
     */
    protected function hasValue(string $item): bool
    {
        return !$this->hasKey($item)
            || $this->getData($item) === ''
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function getData(string $item, $default = NULL)
    {
        return $this->hasKey($item) ? $this->_data[$item] : $default;
    }
}
