<?php

namespace MTi\Input;

use Closure;
use DateTimeZone;
use InvalidArgumentException;
use MTi\Application\SystemMisconfiguredException;
use MTi\DateTime\ExtendedDatetime;
use MTi\DateTime\JulianDate;
use MTi\IDate;
use MTi\IDatetime;
use MTi\InvalidDateException;
use MTi\InvalidNumberException;
use MTi\Type\ExtendedFloat;
use MTi\Type\Money;
use MTi\UnsupportedDateException;
use MTi\UnsupportedDateTimeException;


class InputValueCollector
{
    const MODE_CREATION = 'C';
    const MODE_UPDATE_WHOLE = 'UW';
    const MODE_UPDATE_PART = 'UP';


    /** @var string */
    private $_dt = IInputValueType::TYPE_STRING;

    /** @var bool */
    private $_mandatory = TRUE;

    /** @var int */
    private $_dec = 3;

    /** @var InputValueProcessor */
    private $_vp;

    /** @var Closure|null */
    private $_nullif;

    /** @var array */
    private $_immut = [];


    public function __construct(
        InputValueParser $parser
      , string $mode
      , DateTimeZone $tz
      , string $moneyClassName
      , array $updateDefaults = []
      , string $varnamePrefix = NULL
    )
    {
        if (!in_array($mode, [self::MODE_CREATION, self::MODE_UPDATE_WHOLE, self::MODE_UPDATE_PART])) {
            throw new InvalidArgumentException();
        }
        $this->_parser = $parser;
        $this->_mode = $mode;
        $this->_tz = $tz;
        $this->_mcn = $moneyClassName;
        $this->_defaults = $updateDefaults;
        $this->_varnamePrefix = $varnamePrefix;
    }
    private $_parser;
    private $_mode;
    private $_tz;
    private $_mcn;
    private $_defaults;
    private $_varnamePrefix;
    private $_crtDef;

    private function normalizeValueByType($value, bool $allowNull = TRUE)
    {
        if ($allowNull && is_null($value)) {
            return NULL;
        }
        if (in_array($this->datatype(), [IInputValueType::TYPE_INT, IInputValueType::TYPE_POSITIVE_INT])) {
            if (is_int($value)) {
                return $value;
            }
            if (is_string($value) && ctype_digit($value) && strval($value) < strval(PHP_INT_MAX)) {
                return intval($value);
            }
            throw new InvalidArgumentException(sprintf(
                "Definition value '%s' cannot be recognized as INT."
              , $value
            ));
        }
        elseif ($this->datatype() == IInputValueType::TYPE_LONG) {
            if (is_int($value)) {
                return strval($value);
            }
            if (is_string($value) && preg_match('/^[\d\-]+$/', $value)) {
                return $value;
            }
            throw new InvalidArgumentException(sprintf(
                "Definition value '%s' cannot be recognized as LONG."
              , $value
            ));
        }
        elseif ($this->datatype() == IInputValueType::TYPE_BOOLEAN) {
            if (is_bool($value)) {
                return $value;
            }
            if (is_int($value)) {
                return $value !== 0;
            }
            if (in_array(strtolower($value), ['true', '1'])) {
                return TRUE;
            }
            if (in_array(strtolower($value), ['false', '0'])) {
                return FALSE;
            }
            throw new InvalidArgumentException(sprintf(
                "Definition value '%s' cannot be recognized as BOOL."
              , $value
            ));
        }
        elseif ($this->datatype() == IInputValueType::TYPE_MONEY) {
            if ($value instanceof Money) {
                return $value;
            }
            try {
                $mcn = $this->_mcn;
                return new $mcn($value);
            }
            /** @noinspection PhpRedundantCatchClauseInspection */
            catch (InvalidNumberException $e) {
                throw new InvalidArgumentException(sprintf(
                    "Definition value '%s' cannot be recognized as MONEY."
                  , $value
                ));
            }
        }
        elseif ($this->datatype() == IInputValueType::TYPE_FLOAT) {
            if ($value instanceof ExtendedFloat) {
                return $value;
            }
            try {
                return new ExtendedFloat($value);
            }
            catch (InvalidNumberException $e) {
                throw new InvalidArgumentException(sprintf(
                    "Definition value '%s' cannot be recognized as FLOAT."
                  , $value
                ));
            }
        }
        elseif ($this->datatype() == IInputValueType::TYPE_DATE) {
            if ($value instanceof IDate) {
                return $value;
            }
            try {
                return new JulianDate($value);
            }
            catch (InvalidDateException|UnsupportedDateException $e) {
                throw new InvalidArgumentException(sprintf(
                    "Definition value '%s' cannot be recognized as DATE."
                  , $value
                ));
            }
        }
        elseif ($this->datatype() == IInputValueType::TYPE_DATETIME) {
            if ($value instanceof IDatetime) {
                return $value;
            }
            try {
                return new ExtendedDatetime($value, $this->_tz);
            }
            catch (UnsupportedDateTimeException $e) {
                throw new InvalidArgumentException(sprintf(
                    "Definition value '%s' cannot be recognized as DATETIME."
                  , $value
                ));
            }
        }
        else {
            return $value;
        }
    }

    private function isSameValues($value1, $value2): bool
    {
        $type1 = is_object($value1) ? get_class($value1) : gettype($value1);
        $type2 = is_object($value2) ? get_class($value2) : gettype($value2);
        if ($type1 !== $type2) {
            return FALSE;
        }
        if ($value1 instanceof ExtendedFloat) {
            return !($value1->isGreaterThan($value2) || $value1->isLessThan($value2));
        }
        if ($value1 instanceof IDate) {
            return $value1->equals($value2);
        }
        return $value1 === $value2;
    }

    /**
     * Sets default value used in CREATION mode and missing input value.
     *
     * @param mixed $value
     * @return InputValueCollector
     */
    public function setCreationDefaultValue($value): InputValueCollector
    {
        if ($this->mode() == self::MODE_CREATION) {
            $this->_crtDef = $this->normalizeValueByType($value, FALSE);
        }
        return $this;
    }

    /**
     * Sets required datatype.
     *
     * @param string $type IInputValueType::TYPE_***
     * @return InputValueCollector
     */
    public function setDataType(string $type): InputValueCollector
    {
        $this->_dt = $type;
        if ($this->_crtDef) {
            $this->_crtDef = $this->normalizeValueByType($this->_crtDef);
        }
        foreach ($this->_immut as $key => $immutableValue) {
            $this->_immut[$key] = $this->normalizeValueByType($immutableValue, FALSE);
        }
        return $this;
    }

    /**
     * Value is mandatory?
     *
     * @param bool $value
     * @return InputValueCollector
     */
    public function setMandatory(bool $value): InputValueCollector
    {
        $this->_mandatory = $value;
        return $this;
    }

    public function setValueProcessor(InputValueProcessor $processor): InputValueCollector
    {
        $this->_vp = $processor;
        return $this;
    }

    private function getValueProcessor(): InputValueProcessor
    {
        if (!$this->_vp) {
            $this->_vp = new InputValueProcessor();
        }
        return $this->_vp;
    }

    /**
     * Appends additional check function which can throw InvalidParameterException.
     * Function receives `string $varname, {datatype} $value` and MUST return new value (in most cases: $value).
     * Example:
     *   function (string $varname, ExtendedFloat $value) use ($magicvar)
     *   {
     *       if ($value->isGreaterThan($magicvar)) {
     *           throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
     *       }
     *       return $value;
     *   }
     *
     * @param Closure $logic
     * @param bool $allowNull
     * @return InputValueCollector
     */
    public function addProcessorLogic(Closure $logic, bool $allowNull = FALSE): InputValueCollector
    {
        $this->getValueProcessor()->addLogic($logic, $allowNull);
        return $this;
    }

    /**
     * Collector always returns NULL if this function returns TRUE.
     * Function does not receive any parameters.
     *
     * @param Closure $logic
     * @return InputValueCollector
     */
    public function setNullIf(Closure $logic): InputValueCollector
    {
        $this->_nullif = $logic;
        return $this;
    }

    /**
     * @param array $immutableValues
     * @return InputValueCollector
     */
    public function setImmutableValues(array $immutableValues): InputValueCollector
    {
        $this->_immut = [];
        foreach ($immutableValues as $immutableValue) {
            array_push($this->_immut, $this->normalizeValueByType($immutableValue, FALSE));
        }
        return $this;
    }

    public function setDecimalPlaces(int $number): InputValueCollector
    {
        $this->_dec = $number;
        return $this;
    }

    private function parser(): InputValueParser
    {
        return $this->_parser;
    }

    private function mode(): string
    {
        return $this->_mode;
    }

    private function isMandatory(): bool
    {
        return $this->_mandatory;
    }

    private function isOptional(): bool
    {
        return !$this->isMandatory();
    }

    private function datatype(): string
    {
        return $this->_dt;
    }

    public function hasDefaultValue(string $key): bool
    {
        return array_key_exists($key, $this->_defaults);
    }

    /**
     * @param string $key
     * @param bool $required
     * @return mixed
     */
    public function getDefaultValue(string $key, bool $required = TRUE)
    {
        if (!$this->hasDefaultValue($key)) {
            if ($required) {
                throw new SystemMisconfiguredException(sprintf("Missing default input value for key '%s'.", $key));
            }
            else {
                return NULL;
            }
        }
        $defaultValue = $this->normalizeValueByType($this->_defaults[$key]);
        if (!$required || $defaultValue === FALSE) {
            return $defaultValue;
        }
        if (!is_int($defaultValue) && !is_array($defaultValue) && empty($defaultValue)) {
            throw new SystemMisconfiguredException(sprintf("Empty default input value for key '%s'.", $key));
        }
        return $defaultValue;
    }

    /**
     * @param string $varname
     * @param string|NULL $ifEmptyKey
     * @return mixed
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function gainValue(string $varname, string $ifEmptyKey = NULL)
    {
        if (!is_null($this->_nullif)) {
            if (call_user_func($this->_nullif)) {
                return NULL;
            }
        }
        if (!is_null($this->_varnamePrefix)) {
            $varname = $this->_varnamePrefix . $varname;
        }
        if (!is_null($this->_crtDef)) {
            $default = $this->_crtDef;
        }
        else {
            $default = $ifEmptyKey && $this->hasDefaultValue($ifEmptyKey)
                ? $this->getDefaultValue($ifEmptyKey, $this->isMandatory() && is_null($this->_nullif))
                : NULL
            ;
        }
        try {
            if ($this->datatype() == IInputValueType::TYPE_INT) {
                $value = $this->parser()->getInt($varname, $default);
            }
            elseif ($this->datatype() == IInputValueType::TYPE_POSITIVE_INT) {
                $value = $this->parser()->getPositiveInt($varname, $default);
            }
            elseif ($this->datatype() == IInputValueType::TYPE_LONG) {
                $value = $this->parser()->getBigInt($varname, $default);
            }
            elseif ($this->datatype() == IInputValueType::TYPE_BOOLEAN) {
                $value = $this->parser()->getBool($varname, $default);
            }
            elseif ($this->datatype() == IInputValueType::TYPE_FLOAT) {
                $value = $this->parser()->getFloat($varname, $default, $this->_dec);
            }
            elseif ($this->datatype() == IInputValueType::TYPE_MONEY) {
                $value = $this->parser()->getMoney($varname, $default, $this->_dec);
            }
            elseif ($this->datatype() == IInputValueType::TYPE_DATE) {
                $value = $this->parser()->getDate($varname, $default);
            }
            elseif ($this->datatype() == IInputValueType::TYPE_DATETIME) {
                $value = $this->parser()->getDateTime($varname, $this->_tz, $default);
            }
            else {
                $value = $this->parser()->getString($varname, $default) ?: NULL;
            }
        }
        catch (EmptyParameterException $e) {
            if ($this->isMandatory()) {
                throw $e;
            }
            $value = NULL;
        }
        catch (InvalidParameterException $e) {
            if ($this->isOptional() && $e->getCode() == -2) {
                $value = NULL;
            }
            else {
                throw $e;
            }
        }
        $that = $this;
        if (
            !is_null($default)
            && count(
                array_filter($this->_immut, function ($v) use ($default, $that)
                {
                    return $that->isSameValues($default, $v);
                })
            ) > 0
            && !$this->isSameValues($default, $value)
        ) {
            throw new InvalidParameterException($varname, InvalidParameterException::IMMUTABLE);
        }
        if ($this->_vp) {
            return $this->_vp->process($varname, $value);
        }
        return $value;
    }
}
