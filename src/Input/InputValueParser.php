<?php

namespace MTi\Input;

use DateTimeZone;
use Exception;
use InvalidArgumentException;
use LogicException;
use MTi\DateTime\ExtendedDatetime;
use MTi\DateTime\GregorianDate;
use MTi\DateTime\GregorianMonth;
use MTi\IDate;
use MTi\IDatetime;
use MTi\InvalidDateException;
use MTi\InvalidNumberException;
use MTi\Type\ExtendedFloat;
use MTi\Type\Money;
use MTi\UnsupportedDateException;
use MTi\UnsupportedMonthException;


class InputValueParser
{
    public function __construct(IInputDataProvider $dataProvider, string $moneyClassName)
    {
        $this->_dp = $dataProvider;
        if (!is_subclass_of($moneyClassName, Money::class)) {
            throw new InvalidArgumentException('Not Money implementation.');
        }
        $this->_mcn = $moneyClassName;
    }
    private $_dp;
    private $_mcn;

    /**
     * @param string $varname
     * @param array|NULL $default
     * @return mixed
     * @throws InvalidParameterException
     */
    private function getSecureData(string $varname, array $default = NULL)
    {
        return $this->_dp->getSecureData($varname, $default);
    }

    /**
     * @param string $varname
     * @return mixed
     * @throws InvalidParameterException
     */
    public function getRawData(string $varname)
    {
        return $this->_dp->getUnsecureData($varname);
    }

    /**
     * @param string $varname
     * @param string|null $default
     * @return string
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getString(string $varname, string $default = NULL): string
    {
        $value = $this->getSecureData($varname);
        if (is_null($value)) {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
            return $default;
        }
        $value = strval($value);
        if ($value === '' && $default !== '') {
            throw new EmptyParameterException($varname);
        }
        return strval($value);
    }

    /**
     * @param string $varname
     * @param int $minimalLength
     * @param string|NULL $default
     * @return string
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getStringMinimalLength(string $varname, int $minimalLength, string $default = NULL): string
    {
        if ($default && strlen($default) < $minimalLength) {
            throw new LogicException();
        }
        $s = $this->getString($varname, $default);
        if (strlen($s) < $minimalLength) {
            throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
        }
        return $s;
    }

    /**
     * @param string $varname
     * @param string|null $default
     * @return string
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getBigInt(string $varname, string $default = NULL): string
    {
        $value = $this->getRawData($varname);
        if (is_null($value)) {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
            return $default;
        }
        $value = strval($value);
        if ($value === '') {
            throw new EmptyParameterException($varname);
        }
        if (!preg_match('/^[\d\-]+$/', $value)) {
            throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
        }
        return $value;
    }

    /**
     * @param string $varname
     * @param int|null $default
     * @return int
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getInt(string $varname, int $default = NULL): int
    {
        $value = $this->getRawData($varname);
        if (is_null($value)) {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
            return $default;
        }
        $value = strval($value);
        if ($value === '') {
            throw new EmptyParameterException($varname);
        }
        if ($value[0] === '-') {
            $test = substr($value, 1);
        }
        else {
            $test = &$value;
        }
        if (!ctype_digit($test) || strlen($test) > strlen(strval(PHP_INT_MAX))) {
            throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
        }
        return intval($value);
    }

    /**
     * @param string $varname
     * @param int|null $default
     * @return int
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getPositiveInt(string $varname, int $default = NULL): int
    {
        $int = $this->getInt($varname, $default);
        if ($int <= 0) {
            throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
        }
        return $int;
    }

    /**
     * @param string $varname
     * @param bool|null $default
     * @return bool
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getBool(string $varname, bool $default = NULL): bool
    {
        $value = $this->getRawData($varname);
        if (is_null($value)) {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
            return $default;
        }
        if (is_bool($value)) {
            // Pouhé přetypování na string vrací pro false prázdný string
            $value = ($value ? '1' : '0');
        }
        else {
            $value = strval($value);
        }
        if ($value === '') {
            throw new EmptyParameterException($varname);
        }
        if (strtolower($value) === 'true' || $value === '1') {
            return TRUE;
        }
        if (strtolower($value) === 'false' || $value === '0') {
            return FALSE;
        }
        throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
    }

    /**
     * @param string $varname
     * @param ExtendedFloat|null $default
     * @param int $decimalPlaces
     * @return ExtendedFloat
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getFloat(string $varname, ExtendedFloat $default = NULL, int $decimalPlaces = 3): ExtendedFloat
    {
        $value = $this->getRawData($varname);
        if (is_null($value)) {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
            return $default;
        }
        $value = strval($value);
        if ($value === '') {
            throw new EmptyParameterException($varname);
        }
        try {
            return new ExtendedFloat($value, $decimalPlaces);
        }
        catch (InvalidNumberException $e) {
            throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
        }
    }

    /**
     * @param string $varname
     * @param Money|null $default
     * @param int $decimalPlaces
     * @param string $outDecimalSeparator
     * @param string $outThousandsSeparator
     * @return Money
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getMoney(
        string $varname
      , Money $default = NULL
      , int $decimalPlaces = 2
      , string $outDecimalSeparator = ','
      , string $outThousandsSeparator = ' '
    ): Money
    {
        $value = $this->getRawData($varname);
        if (is_null($value)) {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
            return $default;
        }
        $value = strval($value);
        if ($value === '') {
            throw new EmptyParameterException($varname);
        }
        try {
            $moneyClassName = $this->_mcn;
            return new $moneyClassName($value, $decimalPlaces, $outDecimalSeparator, $outThousandsSeparator);
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (InvalidNumberException $e) {
            throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
        }
    }

    /**
     * @param string $varname
     * @param IDate|null $default
     * @return IDate
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getDate(string $varname, IDate $default = NULL): IDate
    {
        $var = $this->getRawData($varname);
        if (is_null($var)) {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
            return $default;
        }
        try {
            return new GregorianDate($var);
        }
        catch (UnsupportedDateException|InvalidDateException $e) {
            throw new InvalidParameterException($varname,InvalidParameterException::WRONG);
        }
        catch (InvalidArgumentException $e) {
            throw new EmptyParameterException($varname);
        }
    }

    /**
     * @param string $varname
     * @param GregorianMonth|null $default
     * @return GregorianMonth
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getMonth(string $varname, GregorianMonth $default = NULL): GregorianMonth
    {
        $var = $this->getRawData($varname);
        if (is_null($var)) {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
            return $default;
        }
        try {
            if (!preg_match('/^(0?[1-9]|1[012])\/\d{2}$/', $var)) {
                throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
            }
            return new GregorianMonth($var);
        }
        catch (UnsupportedMonthException $e) {
            throw new InvalidParameterException($varname,InvalidParameterException::WRONG);
        }
        catch (InvalidArgumentException $e) {
            throw new EmptyParameterException($varname);
        }
    }

    /**
     * @param string $varname
     * @param DateTimeZone $tz
     * @param IDatetime|null $default
     * @return IDatetime
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    public function getDateTime(string $varname, DateTimeZone $tz, IDatetime $default = NULL): IDatetime
    {
        $value = $this->getRawData($varname);
        if (is_null($value)) {
            if (is_null($default)) {
                throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
            }
            return $default;
        }
        if ($value === '') {
            throw new EmptyParameterException($varname);
        }
        if (!preg_match('/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$/', $value)) {
            throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
        }
        try {
            return new ExtendedDatetime($value, $tz);
        }
        catch (Exception $e) {
            throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
        }
    }

    /**
     * @param string $varname
     * @return bool
     * @throws InvalidParameterException
     */
    public function isArray(string $varname): bool
    {
        return is_array($this->getSecureData($varname));
    }

    /**
     * @param string $varname
     * @param bool $allowEmpty
     * @param array $default
     * @return array
     * @throws InvalidParameterException
     */
    public function getArray(string $varname, bool $allowEmpty = TRUE, array $default = []): array
    {
        $retval = $this->getSecureData($varname, $default);
        if (!is_array($retval)) {
            throw new InvalidParameterException($varname, InvalidParameterException::WRONG);
        }
        if (empty($retval) && !$allowEmpty) {
            throw new InvalidParameterException($varname, InvalidParameterException::MISSING);
        }
        foreach ($default as $k => $v) {
            if (!isset($retval[$k])) {
                $retval[$k] = $v;
            }
        }
        return $retval;
    }
}
