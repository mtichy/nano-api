<?php

namespace MTi\Input;


abstract class ArrayInput
    extends KeyValueInput
{
    public function __construct(array $data)
    {
        $this->_data = $data;
    }
    private $_data;

    /**
     * {@inheritdoc}
     */
    protected function hasKey(string $item): bool
    {
        return array_key_exists($item, $this->_data);
    }

    /**
     * {@inheritdoc}
     */
    protected function hasValue(string $item): bool
    {
        return !$this->hasKey($item)
            || $this->getData($item) === ''
        ;
    }

    /**
     * @param string $item
     * @param null $default
     * @return mixed|null
     */
    protected function getData(string $item, $default = NULL)
    {
        return $this->hasKey($item) ? $this->_data[$item] : $default;
    }
}
