<?php

namespace MTi\Input;

use Exception;
use LogicException;
use MTi\Type\Money;


abstract class FormInput
    extends KeyValueInput
{
    public function __construct(IInputDataProvider $dataProvider, string $moneyClassName, bool $update)
    {
        $this->_dp = $dataProvider;
        $this->_upd = $update;
        $this->_mcn = $moneyClassName;
        $this->_vp = new InputValueParser($dataProvider, $moneyClassName);
    }
    private $_dp;
    private $_vp;
    private $_upd;
    private $_mcn;

    /**
     * @return bool
     */
    final protected function isUpdate(): bool
    {
        return $this->_upd;
    }

    /**
     * @return bool
     */
    final protected function isCreation(): bool
    {
        return !$this->_upd;
    }

    /**
     * @return IInputDataProvider
     */
    protected function dataProvider()
    {
        return $this->_dp;
    }

    final protected function moneyClassName(): string
    {
        return $this->_mcn;
    }

    protected function createZeroMoney(): Money
    {
        $mcn = $this->moneyClassName();
        try {
            /** @var Money $m */
            $m = new $mcn(0);
        }
        catch (Exception $e) {
            throw new LogicException('Every Money implementation must know 0.');
        }
        return $m;
    }

    /**
     * @return InputValueParser
     */
    protected function valueParser(): InputValueParser
    {
        return $this->_vp;
    }

    /**
     * {@inheritdoc}
     */
    protected function hasKey(string $item): bool
    {
        return $this->dataProvider()->hasData($item);
    }

    /**
     * {@inheritdoc}
     */
    protected function hasValue(string $item): bool
    {
        return $this->hasKey($item)
            && ($this->isUpdate() || $this->getData($item) !== '')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function getData(string $item, $default = NULL)
    {
        return $this->dataProvider()->getSecureData($item, $default);
    }
}
