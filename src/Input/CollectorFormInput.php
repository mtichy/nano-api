<?php

namespace MTi\Input;

use DateTimeZone;
use Doctrine\ORM\EntityManager;
use InvalidArgumentException;


abstract class CollectorFormInput
    extends FormInput
{
    public function __construct(
        IInputDataProvider $dataProvider
      , EntityManager $entityManager
      , DateTimeZone $tz
      , string $moneyClassName
      , bool $update
      , array $defaults = []
      , string $varnamePrefix = NULL
    )
    {
        parent::__construct($dataProvider, $moneyClassName, $update);
        $this->_em = $entityManager;
        $this->_tz = $tz;
        $this->_defaults = $defaults;
        $this->_varnamePrefix = $varnamePrefix;
    }
    private $_em;
    private $_tz;
    private $_defaults;
    private $_varnamePrefix;

    public function setDefaults(array $newDefaultValues): CollectorFormInput
    {
        $this->_defaults = $newDefaultValues;
        return $this;
    }

    protected function em(): EntityManager
    {
        return $this->_em;
    }

    protected function createProcessor(): InputValueProcessor
    {
        return new InputValueProcessor();
    }

    protected function createCollector(): InputValueCollector
    {
        if ($this->isCreation()) {
            $mode = InputValueCollector::MODE_CREATION;
        }
        else {
            if (empty($this->_defaults)) {
                $mode = InputValueCollector::MODE_UPDATE_WHOLE;
            }
            else {
                $mode = InputValueCollector::MODE_UPDATE_PART;
            }
        }
        return new InputValueCollector(
            $this->valueParser()
          , $mode
          , $this->_tz
          , $this->moneyClassName()
          , $this->_defaults
          , $this->_varnamePrefix
        );
    }

    /**
     * @param string $varname
     * @param string $entityName
     * @param bool $mandatory
     * @param string|null $defaultVarname
     * @return mixed
     * @throws EmptyParameterException
     * @throws InvalidParameterException
     */
    protected function getEntityById(string $varname, string $entityName, bool $mandatory = TRUE, ?string $defaultVarname = NULL)
    {
        $collector = $this->createCollector()
            ->setMandatory($mandatory)
            ->setValueProcessor(
                $this->createProcessor()
                    ->setEntityCreator($this->em(), $entityName)
            )
        ;
        return $collector->gainValue($varname, $defaultVarname);
    }

    protected function createSubInput(
        string $inputClassName
      , string $varnamePrefix = NULL
      , array $defaults = []
    ): CollectorFormInput
    {
        if (is_subclass_of($inputClassName, CollectorFormInput::class)) {
            return new $inputClassName(
                $this->dataProvider()
              , $this->em()
              , $this->_tz
              , $this->moneyClassName()
              , $this->isUpdate()
              , $defaults
              , $varnamePrefix
            );
        }
        elseif (is_subclass_of($inputClassName, FormInput::class)) {
            return new $inputClassName(
                $this->dataProvider()
              , $this->moneyClassName()
              , $this->isUpdate()
            );
        }
        throw new InvalidArgumentException();
    }
}
