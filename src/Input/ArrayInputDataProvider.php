<?php

namespace MTi\Input;


class ArrayInputDataProvider
    extends BaseInputDataProvider
    implements IInputDataProvider
{

    public function __construct(array $data, array $files = NULL)
    {
        $this->_data = $data;
        $this->_files = $files;
    }

    private $_data;
    private $_files;


    protected function getInputData(): array
    {
        return $this->_data;
    }

    protected function getFiles(): array
    {
        return $this->_files;
    }
}