<?php

namespace MTi\Input;


interface IInputValueType
{
    const TYPE_STRING = 'S';
    const TYPE_INT = 'I';
    const TYPE_LONG = 'L';
    const TYPE_POSITIVE_INT = 'PI';
    const TYPE_FLOAT = 'F';
    const TYPE_MONEY = 'M';
    const TYPE_BOOLEAN = 'B';
    const TYPE_DATE = 'D';
    const TYPE_DATETIME = 'DT';
}
