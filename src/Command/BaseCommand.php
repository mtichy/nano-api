<?php

namespace MTi\Command;

use Doctrine\DBAL\ConnectionException;
use Exception;
use Logger;
use MTi\Application\FatalErrorException;
use MTi\IEnv;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


abstract class BaseCommand
    extends Command
{
    public function __construct(IEnv $ue)
    {
        parent::__construct(NULL);
        $this->_ue = $ue;
        register_shutdown_function([$this, 'onShutdown']);
    }

    private $_ue;

    /**
     * @return IEnv
     */
    protected function env(): IEnv
    {
        return $this->_ue;
    }

    protected function logger(string $name = NULL): Logger
    {
        return $this->env()->logger($name);
    }

    protected function beginTransaction(): void
    {
        $this->env()->em()->beginTransaction();
    }

    protected function commitTransaction(): void
    {
        try {
            $this->env()->em()->commit();
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (ConnectionException $e) {/* nevermind */}
    }

    public function run(InputInterface $input, OutputInterface $output)
    {
        try {
            return parent::run($input, $output);
        }
        catch (Exception $e) {
            $this->logger()->fatal($e->getMessage(), $e);
            throw $e;
        }
    }

    final public function onShutdown()
    {
        $error = error_get_last();
        if ($error !== NULL && ($error['type'] & (E_ERROR | E_USER_ERROR | E_RECOVERABLE_ERROR))) {
            $exception = new FatalErrorException(
                $error['message']
              , 0
              , $error['type']
              , $error['file']
              , $error['line']
              , 'command'
            );
            $this->logger()->fatal('Fatal error! ' . $error['message'], $exception);
            /** @noinspection PhpUnhandledExceptionInspection */
            throw $exception;
        }
    }
}
