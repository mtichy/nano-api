<?php

namespace MTi\Logging;

use Doctrine\DBAL\Logging\SQLLogger;
use Logger;


class QueryLogger
    implements SQLLogger
{
    /**
     * @var Logger
     */
    protected $logger;

    protected $lastStartedAt;

    Public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function startQuery($sql, array $params = NULL, array $types = NULL)
    {
        $this->logger->debug($sql);
        if ($params) {
            $this->logger->debug($params);
        }
        $this->lastStartedAt = microtime(TRUE);
    }

    public function stopQuery()
    {
        $executionTime = round((microtime(TRUE) - $this->lastStartedAt), 3);
        $this->logger->debug('Executed in ' . $executionTime . ' seconds');
    }
}
