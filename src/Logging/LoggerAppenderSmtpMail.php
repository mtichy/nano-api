<?php

namespace MTi\Logging;

use LoggerAppenderMail;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;


class LoggerAppenderSmtpMail
    extends LoggerAppenderMail
{
    protected $smtpHost;
    protected $smtpPort = 25;
    protected $smtpUser;
    protected $smtpPassword;

    protected function getMailer(): Swift_Mailer
    {
        if (!$this->mailer) {
            $transport = (new Swift_SmtpTransport($this->smtpHost, $this->smtpPort, 'tls'));
            if ($this->smtpUser) {
                $transport
                    ->setUsername($this->smtpUser)
                    ->setPassword($this->smtpPassword)
                ;
            }
            $this->mailer = new Swift_Mailer($transport);
        }
        return $this->mailer;
    }
    private $mailer;

    public function close()
    {
        if (!$this->closed) {
            $from = $this->from;
            $to = $this->to;
            if (!empty($this->body) && $from !== NULL && $to !== NULL && $this->layout !== NULL) {
                $subject = @sprintf($this->subject, gethostname());
                if (!$this->dry) {
                    $message = (new Swift_Message($subject))
                        ->setFrom($from)
                        ->setTo($to)
                        ->setBody($this->layout->getHeader() . $this->body . $this->layout->getFooter())
                    ;
                    $this->getMailer()->send($message);
                }
                else {
                    echo "DRY MODE OF MAIL APP.: Send mail to: ".$to." with content: ".$this->body;
                }
            }
            $this->closed = TRUE;
        }
    }

    /**
     * @param mixed $smtpHost
     */
    public function setSmtpHost($smtpHost)
    {
        $this->setString('smtpHost', $smtpHost);
    }

    /**
     * @param int $smtpPort
     */
    public function setSmtpPort(int $smtpPort)
    {
        $this->setInteger('smtpPort', $smtpPort);
    }

    /**
     * @param mixed $smtpUser
     */
    public function setSmtpUser($smtpUser)
    {
        $this->setString('smtpUser', $smtpUser);
    }

    /**
     * @param mixed $smtpPassword
     */
    public function setSmtpPassword($smtpPassword)
    {
        $this->setString('smtpPassword', $smtpPassword);
    }
}
