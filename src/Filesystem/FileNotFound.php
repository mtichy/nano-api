<?php

namespace MTi\Filesystem;

use MTi\IOException;


class FileNotFound
    extends IOException
{
    public function __construct($file)
    {
        parent::__construct(sprintf(
            "File '%s' does not exists."
          , $file
        ));
    }
}
