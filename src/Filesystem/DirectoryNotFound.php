<?php

namespace MTi\Filesystem;

use MTi\IOException;


class DirectoryNotFound
    extends IOException
{
    public function __construct($dir)
    {
        parent::__construct(sprintf(
            "Directory '%s' does not exists."
            , $dir
        ));
    }
}
