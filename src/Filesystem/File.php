<?php

namespace MTi\Filesystem;

use MTi\IOException;


class File
{
    private static $mimeRegexp = '/^([a-z\-]+\/[a-z0-9\-\.\+]+)(;\s.+)?$/';

    /** @var string */
    private $fullName;
    /** @var string */
    private $name;
    /** @var string */
    private $extension;
    /** @var string */
    private $dir;
    /** @var int */
    private $size;
    /** @var string */
    private $file;
    /** @var string */
    private $contentType;

    /**
     * @param string $filePath
     * @param string|null $contentType
     * @throws FileNotFound
     */
    public function __construct(string $filePath, string $contentType = NULL)
    {
        if (!is_file($filePath)) {
            throw new FileNotFound($filePath);
        }
        $pathParts = pathinfo($filePath);
        $this->fullName = $pathParts['basename'];
        $this->name = $pathParts['filename'];
        $this->extension = $pathParts['extension'] ?? '';
        $this->dir = $pathParts['dirname'];
        $this->file = $filePath;
        $this->contentType = $contentType;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @return string
     */
    public function getDir(): string
    {
        return $this->dir;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    public function getContentType(): string
    {
        if ($this->contentType === NULL) {
            $finfo = finfo_open(FILEINFO_MIME);
            if (is_resource($finfo)) {
                $mime = finfo_file($finfo, $this->getFile());
                finfo_close($finfo);
                /* According to the comments section of the PHP manual page,
                 * it is possible that this function returns an empty string
                 * for some files (e.g. if they don't exist in the magic MIME database)
                 */
                if (is_string($mime) && preg_match(self::$mimeRegexp, $mime, $matches)) {
                    return $matches[1];
                }
            }
            return 'application/octet-stream';
        }
        return $this->contentType;
    }

    public function getSize(): int
    {
        if ($this->size === NULL) {
            $this->size = filesize($this->file);
        }
        return $this->size;
    }

    /**
     * @return string
     * @throws IOException
     */
    public function getContents()
    {
        $content = @file_get_contents($this->getFile()); // @ is escalated to exception
        if ($content === false) {
            throw new IOException(sprintf("Unable to read file '%s'.", $this->getFile()));
        }
        return $content;
    }
}
