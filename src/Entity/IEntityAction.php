<?php

namespace MTi\Entity;


interface IEntityAction
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return array [param_name] => [param_value]
     */
    public function getParameters(): array;
}
