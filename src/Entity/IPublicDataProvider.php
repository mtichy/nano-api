<?php

namespace MTi\Entity;


interface IPublicDataProvider
{
    /**
     * Returns data, where it does not matter to send outside the internal corporate network.
     *
     * @return array
     */
    public function getPublicData(): array;
}
