<?php

namespace MTi\Entity;


interface ITableKeyProvider
{
    const ARRAY_INDEX_KEY = '_table_key_';

    /**
     * Returns entity unique key for loading into table - most important in entities with composite key.
     *
     * @return string
     */
    public function getReviewTableKey(): string;
}
