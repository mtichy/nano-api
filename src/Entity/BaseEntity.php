<?php

namespace MTi\Entity;

use DateTime;
use InvalidArgumentException;
use JsonSerializable;
use LogicException;
use MTi\DateTime\ExtendedDatetime;
use MTi\IDate;
use MTi\IDatetime;
use MTi\InvalidNumberException;
use MTi\RegexpException;
use MTi\Type\ExtendedFloat;
use MTi\Type\Money;
use MTi\Type\UnicodeString;
use MTi\UnsupportedDateTimeException;
use MTi\Util\Json;
use Nette\Utils\JsonException;


abstract class BaseEntity
    implements JsonSerializable
{
    /**
     * @param mixed $value
     * @param int $decimals
     * @return ExtendedFloat|null
     */
    protected function convertToApplicationFloat($value, int $decimals = 6)
    {
        if (is_null($value) || $value === '') {
            return NULL;
        }
        if (is_string($value) && $value[0] === '.') {
            $value = '0'.$value;
        }
        try {
            return new ExtendedFloat($value, $decimals);
        }
        catch (InvalidNumberException $e) {
            throw new InvalidArgumentException();
        }
    }

    /**
     * @param mixed $value
     * @param string $moneyClassName
     * @return Money|null
     */
    protected function convertToMoney($value, string $moneyClassName): ?Money
    {
        if (!is_subclass_of($moneyClassName, Money::class)) {
            throw new InvalidArgumentException('Not Money implementation.');
        }
        if (is_string($value) && $value[0] === '.') {
            $value = '0'.$value;
        }
        try {
            return (is_null($value) || $value === '')
                ? NULL
                : new $moneyClassName($value)
            ;
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (InvalidNumberException $e) {
            throw new InvalidArgumentException();
        }
    }

    /**
     * @param DateTime $dateTime
     * @return IDatetime|null
     */
    protected function convertToApplicationDateTime(DateTime $dateTime = NULL): ?IDatetime
    {
        if (is_null($dateTime)) {
            return NULL;
        }
        try {
            return new ExtendedDatetime($dateTime, $dateTime->getTimezone());
        }
        catch (UnsupportedDateTimeException $e) {
            throw new LogicException();
        }
    }

    /**
     * @param IDate|null $date
     * @return DateTime|null
     */
    protected function gainDoctrineDate(?IDate $date): ?DateTime
    {
        return $date ? $date->asPhpDateTime() : NULL;
    }

    /**
     * @param IDatetime|null $datetime
     * @return DateTime|null
     */
    protected function gainDoctrineDatetime(?IDatetime $datetime): ?DateTime
    {
        return $datetime ? $datetime->asPhpDateTime() : NULL;
    }

    /**
     * @param ExtendedFloat|null $float
     * @return string|null
     */
    protected function gainDoctrineDecimal(?ExtendedFloat $float): ?string
    {
        if (is_null($float)) {
            return NULL;
        }
        $val = rtrim(rtrim(strval($float), '0'), '.');
        if ($val === '0') {
            return $val;
        }
        return ltrim($val, '0');
    }

    /**
     * Returns full entity data.
     *
     * @return array
     */
    abstract protected function getFullData(): array;

    /**
     * Returns data subset for reviews. It typically contains human readable items.
     *
     * @return array
     */
    protected function getReviewData(): array
    {
        return $this->getFullData();
    }

    /**
     * @param string $prefix
     * @return array
     */
    final public function getReview(string $prefix = ''): array
    {
        if (empty($prefix)) {
            return $this->getReviewData();
        }
        $review = [];
        foreach ($this->getReviewData() as $k => $v) {
            $review[$prefix.$k] = $v;
        }
        return $review;
    }

    /**
     * Vrací mapu položek review, které se mají zobrazit při aktualizaci property.
     * Např:
     *   'validSince' => ['valid_since', 'year']
     * Vhodné je definovat zde property, jejíž změna má vliv na vícero položek review, nebo má specifický název.
     *
     * @return array
     */
    protected function getReviewChangeMap(): array
    {
        return [];
    }

    /**
     * Na základě seznamu změněných položek entity vrací review položek ovlivněných změnou.
     *
     * @param array $changelist
     * @return array
     */
    final public function getReviewDiff(array $changelist): array
    {
        $changeMap = $this->getReviewChangeMap();
        $mask = [];
        foreach ($changelist as $changedPropName) {
            if (array_key_exists($changedPropName, $changeMap)) {
                foreach ($changeMap[$changedPropName] as $key) {
                    $mask[$key] = TRUE;
                }
            }
            else {
                $sChPropName = new UnicodeString($changedPropName);
                // review item name typically equals property name in entity coverted to "snake_case"
                try {
                    $mask[strval($sChPropName->toSnakeCase())] = TRUE;
                }
                catch (RegexpException $ignore) {/* nevermind */}
            }
        }
        $changedKeys = array_keys($mask);
        return array_filter($this->getReview(), function ($key) use ($changedKeys) {
             return in_array($key, $changedKeys);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @return array
     * @throws JsonException
     */
    final public function jsonSerialize()
    {
        $d = $this->getFullData();
        Json::ensureJson($d);
        return $d;
    }
}
