<?php

namespace MTi\Entity;

use ArrayIterator;
use JsonSerializable;
use Nette\Utils\JsonException;


class EntityList
    extends ArrayIterator
    implements JsonSerializable
{
    /**
     * @return array
     * @throws JsonException
     */
    final public function jsonSerialize()
    {
        $list = [];
        foreach ($this as $key => $obj) {
            if (is_array($obj)) {
                $list[$key] = new EntityList($obj);
            }
            else {
                /** @var BaseEntity $obj */
                array_push($list, $obj->jsonSerialize());
            }
        }
        return $list;
    }
}
