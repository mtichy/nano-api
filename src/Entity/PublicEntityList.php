<?php

namespace MTi\Entity;

use ArrayIterator;
use JsonSerializable;
use Nette\Utils\JsonException;


class PublicEntityList
    extends ArrayIterator
    implements JsonSerializable
{
    private function serializeField($value)
    {
        if ($value instanceof JsonSerializable) {
            return $value->jsonSerialize();
        }
        if (is_array($value)) {
            return $this->serializeData($value);
        }
        return $value;
    }

    private function serializeData(array $data)
    {
        return array_map([$this, 'serializeField'], $data);
    }

    /**
     * @return array
     * @throws JsonException
     */
    final public function jsonSerialize()
    {
        $list = [];
        foreach ($this as $obj) { /** @var IPublicDataProvider $obj */
            if (!($obj instanceof IPublicDataProvider)) {
                throw new JsonException("Not a public data provider!");
            }
            array_push($list, $this->serializeData($obj->getPublicData()));
        }
        return $list;
    }
}
