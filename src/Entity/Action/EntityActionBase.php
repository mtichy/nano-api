<?php

namespace MTi\Entity\Action;

use JsonSerializable;
use LogicException;
use MTi\Entity\IEntityAction;
use MTi\Entity\ITableKeyProvider;
use MTi\Util\Json;
use Nette\Utils\JsonException;


abstract class EntityActionBase
    implements IEntityAction, JsonSerializable
{
    public function __construct(
        string $name
      , string $title
      , bool $groupable
      , array $parameters = []
      , string $tableKey = NULL
    )
    {
        $this->_name = $name;
        $this->_title = $title;
        $this->_groupable = $groupable;
        foreach ($parameters as $paramName => $value) {
            $this->setParameter($paramName, $value);
        }
        $this->_tblKey = $tableKey;
    }
    private $_name;
    private $_title;
    private $_groupable;
    private $_parameters = [];
    private $_tblKey;

    /**
     * {@inheritdoc}
     */
    final public function getName(): string
    {
        return $this->_name;
    }

    /**
     * {@inheritdoc}
     */
    final public function getTitle(): string
    {
        return $this->_title;
    }

    final public function isGroupable()
    {
        return $this->_groupable;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    final protected function setParameter(string $name, $value)
    {
        $this->_parameters[$name] = $value;
    }

    /**
     * {@inheritdoc}
     */
    final public function getParameters(): array
    {
        return $this->_parameters;
    }

    /**
     * Override in cases, where name comparison is not enough.
     *
     * @param EntityActionBase $action
     * @return bool
     */
    public function isSame(EntityActionBase $action): bool
    {
        return $this->getTitle() === $action->getTitle();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function jsonSerialize()
    {
        $params = $this->getParameters();
        try {
            Json::ensureJson($params);
        }
        catch (JsonException $e) {
            throw new LogicException();
        }
        $def = [
            'action' => $this->getName(),
            'title' => $this->getTitle(),
            'params' => $params,
        ];
        if ($this->_tblKey) {
            $def[ITableKeyProvider::ARRAY_INDEX_KEY] = $this->_tblKey;
        }
        return $def;
    }
}
