<?php

namespace MTi\Entity\Action;


use JsonSerializable;


class EntityGroupActionList
    implements JsonSerializable
{
    /**
     * @var EntityActionList
     */
    private $_list;

    public function addAction(EntityActionBase $action, bool $clearSameName = FALSE): EntityGroupActionList
    {
        if (is_null($this->_list)) {
            $this->_list = new EntityActionList();
        }
        if ($clearSameName) {
            $this->_list->clearActionsByName($action->getName());
        }
        $this->_list->addAction($action);
        return $this;
    }

    /**
     * @param EntityActionList $entityActionList
     * @return EntityGroupActionList
     */
    public function mergeActionList(EntityActionList $entityActionList): EntityGroupActionList
    {
        $list = clone $entityActionList;
        $list->onlyGroupable();
        if (is_null($this->_list)) {
            $this->_list = $list;
        }
        else {
            $this->_list->intersect($list);
        }
        return $this;
    }

    /**
     * @param EntityActionSectionList $entityActionSectionList
     * @return EntityGroupActionList
     */
    public function mergeActionSectionList(EntityActionSectionList $entityActionSectionList): EntityGroupActionList
    {
        return $this->mergeActionList($entityActionSectionList->getFlatList());
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        if (is_null($this->_list)) {
            return [];
        }
        return $this->_list->getDividedByTitle();
    }
}
