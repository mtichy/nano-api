<?php

namespace MTi\Entity\Action;

use ArrayAccess;
use InvalidArgumentException;
use JsonSerializable;


class EntityActionSectionList
    implements JsonSerializable, ArrayAccess
{
    private $_sections = [];

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->_sections);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset)) {
            throw new InvalidArgumentException();
        }
        return $this->_sections[$offset];
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->_sections[$offset] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->_sections[$offset]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return $this->_sections;
    }

    /**
     * @param string $sectionName
     * @param EntityActionList $actions
     * @return EntityActionSectionList
     */
    public function addSection(string $sectionName, EntityActionList $actions): EntityActionSectionList
    {
        $this[$sectionName] = $actions;
        return $this;
    }

    public function getFlatList(): EntityActionList
    {
        $flat = new EntityActionList();
        /** @var EntityActionList $section */
        foreach ($this->_sections as $section) {
            $flat->merge($section);
        }
        return $flat;
    }
}
