<?php

namespace MTi\Entity\Action;

use Countable;
use JsonSerializable;
use MTi\Entity\IEntityAction;


class EntityActionList
    implements Countable, JsonSerializable
{
    /**
     * Unique action index to prevent collision with other actions with same type in one array.
     *
     * @param IEntityAction $a
     * @return string
     */
    public static function getActionIndex(IEntityAction $a): string
    {
        return md5(sprintf('%s(%s)', $a->getName(), serialize($a->getParameters())));
    }

    private $_actions = [];

    /**
     * @return EntityActionBase[]
     */
    final protected function getActionArray(): array
    {
        return $this->_actions;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return array_values($this->getActionArray());
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return count($this->_actions);
    }

    /**
     * @param IEntityAction $action
     * @return EntityActionList
     */
    public function addAction(IEntityAction $action): EntityActionList
    {
        $this->_actions[self::getActionIndex($action)] = $action;
        return $this;
    }

    /**
     * @param string $name
     */
    public function clearActionsByName(string $name)
    {
        /**
         * @var string $index
         * @var EntityActionBase $action
         */
        foreach ($this->_actions as $index => $action) {
            if ($action->getName() == $name) {
                unset($this->_actions[$index]);
            }
        }
    }

    /**
     * @param EntityActionBase $action
     * @return EntityActionBase|null
     */
    protected function findSame(EntityActionBase $action)
    {
        /** @var EntityActionBase $a */
        foreach ($this->_actions as $a) {
            if ($a->isSame($action)) {
                return $a;
            }
        }
        return NULL;
    }

    public function intersect(EntityActionList $list): EntityActionList
    {
        $oldActions = $this->_actions;
        $this->_actions = [];
        foreach ($oldActions as $a1) {
            $a2 = $list->findSame($a1);
            if ($a2) {
                $this->addAction($a1);
                $this->addAction($a2);
            }
        }
        return $this;
    }

    public function merge(EntityActionList $list): EntityActionList
    {
        foreach ($list->getActionArray() as $a) {
            if (!$this->findSame($a)) {
                $this->addAction($a);
            }
        }
        return $this;
    }

    /**
     * @return EntityActionList
     */
    public function onlyGroupable(): EntityActionList
    {
        $this->_actions = array_filter($this->_actions, function ($a) { /** @var EntityActionBase $a */
            return $a->isGroupable();
        });
        return $this;
    }

    public function getDividedByTitle(): array
    {
        $sections = [];
        /** @var EntityActionBase $action */
        foreach ($this->_actions as $action) {
            if (!array_key_exists($action->getTitle(), $sections)) {
                $sections[$action->getTitle()] = [
                    'title' => $action->getTitle(),
                    'actions' => [],
                ];
            }
            array_push($sections[$action->getTitle()]['actions'], $action);
        }
        return array_values($sections);
    }
}
