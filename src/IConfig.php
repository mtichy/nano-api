<?php

namespace MTi;

use MTi\Config\MissingConfigItem;


interface IConfig
{
    public const KEY_SEPARATOR = '.';
    public const SECTION_SEPARATOR = ' < ';
    public const RAW_SECTION = '!';

    /**
     * @return array
     */
    public function config(): array;

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function get(string $key, $default = null);

    /**
     * @param string $key
     * @return mixed
     * @throws MissingConfigItem
     */
    public function need(string $key);

    public function toArray(): array;
}
