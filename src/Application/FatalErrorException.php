<?php

namespace MTi\Application;

use ErrorException;


class FatalErrorException
    extends ErrorException
{
    public function __construct($message, $code, $severity, $file, $line, $context)
    {
        parent::__construct($message, $code, $severity, $file, $line);
        $this->context = $context;
    }
    private $context;

    public function getContext()
    {
        return $this->context;
    }
}
