<?php

namespace MTi\Application;

use LogicException;


class SystemMisconfiguredException
    extends LogicException
{
}
