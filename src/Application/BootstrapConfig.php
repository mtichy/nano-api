<?php

namespace MTi\Application;

use LogicException;


class BootstrapConfig
{
    private static $instance = NULL;

    public static function createConfig()
    {
        if (!is_null(self::$instance)) {
            throw new LogicException('Bootstrap config already created!');
        }
        self::$instance = new BootstrapConfig();
        return self::$instance;
    }

    public static function getConfig()
    {
        if (is_null(self::$instance)) {
            return self::createConfig();
        }
        return self::$instance;
    }

    public $configFile = 'config';
    public $displayErrors = 'off';
    public $logFile = NULL;
    public $namespace = '';
    public $useContainer = TRUE;
    public $envClass = NULL;
    public $envParameters = FALSE;
    public $envExtraParameters = FALSE;
    public $ignoreMissingImports = FALSE;
    public $ignoreMissingConfigValues = FALSE;
    public $containerServicesPreparedCallbacks = [];

    /**
     * @param string $configFile
     * @return BootstrapConfig
     */
    public function setConfigFile(string $configFile): BootstrapConfig
    {
        $this->configFile = $configFile;
        return $this;
    }

    /**
     * @param string $displayErrors
     * @return BootstrapConfig
     */
    public function setDisplayErrors(string $displayErrors): BootstrapConfig
    {
        $this->displayErrors = $displayErrors;
        return $this;
    }

    /**
     * @param null $logFile
     * @return BootstrapConfig
     */
    public function setLogFile($logFile)
    {
        $this->logFile = $logFile;
        return $this;
    }

    /**
     * @param string $namespace
     * @return BootstrapConfig
     */
    public function setNamespace(string $namespace): BootstrapConfig
    {
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * @param bool $useContainer
     * @return BootstrapConfig
     */
    public function setUseContainer(bool $useContainer): BootstrapConfig
    {
        $this->useContainer = $useContainer;
        return $this;
    }

    /**
     * @param null $envClass
     * @return BootstrapConfig
     */
    public function setEnvClass($envClass)
    {
        $this->envClass = $envClass;
        return $this;
    }

    /**
     * @param mixed $envExtraParameters
     * @return BootstrapConfig
     */
    public function setEnvExtraParameters($envExtraParameters): BootstrapConfig
    {
        $this->envExtraParameters = $envExtraParameters;
        return $this;
    }

    /**
     * @param mixed $envParameters
     * @return BootstrapConfig
     */
    public function setEnvParameters($envParameters): BootstrapConfig
    {
        $this->envParameters = $envParameters;
        return $this;
    }

    /**
     * @param bool $ignoreMissingImports
     * @return BootstrapConfig
     */
    public function setIgnoreMissingImports(bool $ignoreMissingImports): BootstrapConfig
    {
        $this->ignoreMissingImports = $ignoreMissingImports;
        return $this;
    }

    /**
     * @param bool $ignoreMissingConfigValues
     * @return BootstrapConfig
     */
    public function setIgnoreMissingConfigValues(bool $ignoreMissingConfigValues): BootstrapConfig
    {
        $this->ignoreMissingConfigValues = $ignoreMissingConfigValues;
        return $this;
    }

    /**
     * @param callable $containerServicesPreparedCallback
     * @return BootstrapConfig
     */
    public function addContainerServicesPreparedCallback(callable  $containerServicesPreparedCallback): BootstrapConfig
    {
        $this->containerServicesPreparedCallbacks[] = $containerServicesPreparedCallback;
        return $this;
    }
}
