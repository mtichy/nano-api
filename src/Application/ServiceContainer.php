<?php

namespace MTi\Application;

use DateTimeZone;
use Logger;
use LogicException;
use MTi\DateTime\DateTime;
use MTi\DateTime\GregorianDate;
use MTi\Entity\BaseEntity;
use MTi\IDate;
use MTi\IDatetime;
use MTi\IEnv;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;
use MTi\Util\EnvWrapper;


abstract class ServiceContainer
{
    public function __construct(IEnv $e)
    {
        $this->_ue = $e;
    }
    private $_ue;

    protected function logger(string $name = NULL): Logger
    {
        return $this->_ue->logger($name);
    }

    /**
     * @param string $name
     * @return EnvWrapper
     */
    final protected function createEnvWrapper(string $name): EnvWrapper
    {
        return $this->_ue->getWrapperByName($name);
    }

    /**
     * @return IDate
     */
    final protected function today(): IDate
    {
        return $this->_ue->currentDate();
    }

    /**
     * @return IDatetime
     */
    final protected function now(): IDatetime
    {
        return $this->_ue->currentDatetime();
    }

    final protected function timezone(): DateTimeZone
    {
        return $this->_ue->getDateTimeZone();
    }

    final protected function createDate(int $year, int $month, int $day): GregorianDate
    {
        try {
            return new GregorianDate($day, $month, $year);
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new LogicException();
        }
    }

    final protected function computeChangeset(BaseEntity $entity): array
    {
        $uow = $this->_ue->em()->getUnitOfWork();
        $uow->computeChangeSets();
        $changeset = [];
        foreach ($uow->getEntityChangeSet($entity) as $item => $vals) {
            list($old, $new) = $vals;
            /** @var \DateTime $old */
            if ($new instanceof DateTime && !is_null($old)) {
                if ($old->format('c') == $new->format('c')) {
                    continue;
                }
            }
            array_push($changeset, $item);
        }
        return $changeset;
    }
}
