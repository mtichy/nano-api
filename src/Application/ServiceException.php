<?php

namespace MTi\Application;

use RuntimeException;
use Throwable;


class ServiceException
    extends RuntimeException
{
    public function __construct(string $code, string $message = NULL, Throwable $previous = NULL)
    {
        parent::__construct($message, 0, $previous);
        $this->_code = strtoupper($code);
    }
    private $_code;

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->_code;
    }
}
