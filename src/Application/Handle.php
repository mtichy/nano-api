<?php

namespace MTi\Application;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use InvalidArgumentException;
use MTi\Entity\BaseEntity;
use MTi\IEnv;


abstract class Handle
    extends ServiceContainer
{
    public function __construct(IEnv $e, string $entityClassName)
    {
        if (!is_subclass_of($entityClassName, BaseEntity::class)) {
            throw new InvalidArgumentException();
        }
        parent::__construct($e);
        $this->_ue = $e;
        $this->_cn = $entityClassName;
    }
    private $_ue;
    private $_cn;

    private function em(): EntityManager
    {
        return $this->_ue->em();
    }

    final protected function getRepository(): EntityRepository
    {
        return $this->em()->getRepository($this->_cn);
    }

    final protected function persistEntity(BaseEntity $entity)
    {
        $this->em()->persist($entity);
    }

    final protected function refreshEntity(BaseEntity $entity)
    {
        $this->em()->refresh($entity);
    }

    final protected function removeEntity(BaseEntity $entity)
    {
        $this->em()->remove($entity);
    }

    /**
     * @param BaseEntity|NULL $entity
     */
    final protected function flushChanges(BaseEntity $entity = NULL)
    {
        $this->em()->flush();
        if ($entity) {
            $this->refreshEntity($entity);
        }
    }
}
