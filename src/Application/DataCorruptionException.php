<?php

namespace MTi\Application;

use LogicException;


class DataCorruptionException
    extends LogicException
{
}
