<?php

namespace MTi\Application;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use MTi\IConfig;
use MTi\IEnv;
use MTi\Security\IIdentity;
use Nette\Caching\Cache;
use Nette\IOException;
use Nette\Utils\FileSystem;


abstract class Service
    extends ServiceContainer
{
    public function __construct(IEnv $e)
    {
        parent::__construct($e);
        $this->_ue = $e;
    }
    private $_ue;

    private function em(): EntityManager
    {
        return $this->_ue->em();
    }

    /**
     * @param string|null $section
     * @return IConfig
     */
    final protected function config(string $section = null): IConfig
    {
        return $this->_ue->config($section);
    }

    final protected function cache(string $namespace): Cache
    {
        return $this->_ue->cache($namespace);
    }

    final protected function isProductionEnv(): bool
    {
        return $this->_ue->isProduction();
    }

    final protected function isDevelopmentEnv(): bool
    {
        return $this->_ue->isDevelopment();
    }

    final protected function getLogSubdir(string $subdirName)
    {
        $dir = $this->_ue->logDir() . DIRECTORY_SEPARATOR . $subdirName;
        if (!file_exists($dir)) {
            try {
                FileSystem::createDir($dir, 0770);
            }
            catch (IOException $e) {
                throw new SystemMisconfiguredException("Cannot write to log dir!");
            }
        }
        return $dir;
    }

    final protected function user(): IIdentity
    {
        return $this->_ue->user();
    }

    final protected function getRepository(string $className): EntityRepository
    {
        /** @var EntityRepository $repo */
        $repo = $this->em()->getRepository($className);
        return $repo;
    }

    final protected function qb(): QueryBuilder
    {
        return $this->em()->createQueryBuilder();
    }

    /**
     * @param string $className
     * @param string $alias
     * @return QueryBuilder
     */
    final protected function qba(string $className, string $alias): QueryBuilder
    {
        return $this->getRepository($className)->createQueryBuilder($alias);
    }

    final protected function createQuery(string $sql): Query
    {
        return $this->em()->createQuery($sql);
    }

    /**
     * @param string $sql
     * @return Statement
     * @throws DBALException
     */
    final protected function prepareQuery(string $sql): Statement
    {
        return $this->em()->getConnection()->prepare($sql);
    }
}
