<?php

namespace MTi\Application;

use BadMethodCallException;
use Doctrine\DBAL\ConnectionException;
use Exception;
use LogicException;
use MTi\Controller\BaseController;
use MTi\Controller\ErrorController;
use MTi\Controller\LoginController;
use MTi\Http\HttpException;
use MTi\Http\IResponse;
use MTi\IEnv;
use MTi\Input\InvalidParameterException;
use MTi\ItemNotFound;
use MTi\NotSupportedException;
use MTi\Security\AuthenticationException;
use MTi\Security\UnauthorizedException;
use MTi\Type\UnicodeString;
use Symfony\Component\DependencyInjection\ContainerBuilder;


class Runner
{
    public function __construct(
        IEnv $ue
      , ContainerBuilder $container
      , string $controllerNS
      , string $fixedDomain = NULL)
    {
        $this->_ue = $ue;
        $this->_ctlNs = $controllerNS;
        $this->_fixedDomain = $fixedDomain;
        $this->container = $container;
        register_shutdown_function([$this, 'onShutdown']);
    }
    private $_ue;
    private $_ctlNs;
    private $_fixedDomain;
    private $container;

    /**
     * @return IEnv
     */
    private function env()
    {
        return $this->_ue;
    }

    private function logger()
    {
        return $this->env()->logger();
    }

    final public function onShutdown()
    {
        $error = error_get_last();
        if ($error !== NULL && ($error['type'] & (E_ERROR | E_USER_ERROR | E_RECOVERABLE_ERROR))) {
            $this->logger()->fatal('Fatal error! '.$error['message'], new FatalErrorException(
                $error['message']
              , 0
              , $error['type']
              , $error['file']
              , $error['line']
              , 'appl-runner'
            ));
            try {
                $this->env()->em()->rollback();
            }
            /** @noinspection PhpRedundantCatchClauseInspection */
            catch (ConnectionException $e) {/* není v transakci */}
            $this->quitWithError(
                IResponse::S500_INTERNAL_SERVER_ERROR
              , 'INTERNAL_ERROR'
              , sprintf('%s line %d: %s', $error['file'], $error['line'], $error['message'])
            );
        }
    }

    private function handleCORS()
    {
        try {
            $this->env()->response()->addHeader('Access-Control-Allow-Origin', '*');
            if ($this->env()->request()->getMethod() == 'OPTIONS') {
                $this->env()->response()
                    ->addHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE')
                    ->addHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With')
                ;
                exit(0);
            }
        }
        catch (HttpException $e) {
            throw new LogicException();
        }
    }

    private function quitWithError(
        int $httpCode
      , string $errorCode
      , string $message = NULL
      , string $file = NULL
      , int $line = NULL
    )
    {
        $ctl = new ErrorController($this->env());
        $ctl->quit($httpCode, $errorCode, $message, $file, $line);
    }

    public function run()
    {
        $this->handleCORS();

        if ($this->env()->isInMaintenance()) {
            $this->quitWithError(IResponse::S503_SERVICE_UNAVAILABLE, 'UNDER_MAINTENANCE');
        }

        if ($this->_fixedDomain) {
            $domain = $this->_fixedDomain;
        }
        else {
            $domain = $_GET['__domain'] ?? NULL;
            if (!$domain) {
                $this->quitWithError(IResponse::S404_NOT_FOUND, 'NOT_FOUND');
            }
        }
        $sCtlName = new UnicodeString($domain);
        try {
            $ctlClassName = sprintf(
                '%s\\Controller\\%sController'
              , $this->_ctlNs
              , strval($sCtlName->replace('/-/', ' ')->capitalize()->replace('/ /', ''))
            );
            $ctlShortClassName = sprintf(
                '%sController'
              , strval($sCtlName->replace('/-/', ' ')->capitalize()->replace('/ /', ''))
            );
            if (!class_exists($ctlClassName)) {
                $this->quitWithError(IResponse::S404_NOT_FOUND, 'NOT_FOUND');
            }
            $ctl = $this->container->get($ctlShortClassName);
            if ($ctl instanceof LoginController) {
                $method = $action = 'login';
            }
            else {
                if (!isset($_GET['__action'])) {
                    $this->quitWithError(IResponse::S404_NOT_FOUND, 'NOT_FOUND');
                }
                $action = $_GET['__action'] ?: 'entity';
                $parts = preg_split('/-/', $action);
                if (count($parts) == 1) {
                    $method = $action;
                }
                else {
                    $sMethodPart = new UnicodeString(join(' ', array_slice($parts, 1)));
                    $method = $parts[0] . strval($sMethodPart->replace('/ /', ''));
                }
            }
        }
        catch (Exception $e) {
            $this->logger()->fatal($e->getMessage(), $e);
            $this->quitWithError(
                IResponse::S500_INTERNAL_SERVER_ERROR
              , 'FATAL'
              , $e->getMessage()
              , $e->getFile()
              , $e->getLine()
            );
            return; // aby PHP Storm rozuměl tomu, že zde končíme
        }
        $this->dispatch($ctl, $method, $action, $domain);

    }

    /**
     * @param BaseController $controller
     * @param string $method
     * @param string $action
     * @param string $domain
     */
    private function dispatch(BaseController $controller, string $method, string $action, string $domain): void
    {
        $requestMethod = $this->env()->request()->getMethod();
        $specificMethod = $method . ucfirst(strtolower($requestMethod));
        if (!(method_exists($controller, $method) || method_exists($controller, $specificMethod))) {
            $this->logger()->error(sprintf(
                "Missing API implementation: %s: /%s/%s/"
              , $requestMethod
              , $domain
              , $action
            ));
            $this->quitWithError(IResponse::S501_NOT_IMPLEMENTED, 'NOT_IMPLEMENTED');
        }
        if (method_exists($controller, $specificMethod)) {
            $method = $specificMethod;
        }
        try {
            $controller->beforeDispatch($method);
            $controller->$method();
        }
        catch (NotSupportedException $e) {
            $this->logger()->warn(sprintf(
                "Trying to call unsupported API %s: /%s/%s/"
              , $requestMethod
              , $domain
              , $action
            ));
            $this->quitWithError(IResponse::S405_METHOD_NOT_ALLOWED, 'NOT_ALLOWED');
            exit;
        }
        catch (UnauthorizedException $e) {
            $this->logger()->warn('NOT_AUTHORIZED: ' . $e->getMessage());
            $this->quitWithError(IResponse::S403_FORBIDDEN, 'NOT_AUTHORIZED', $e->getMessage());
        }
        catch (AuthenticationException $e) {
            $this->logger()->warn('AUTH_ERROR: ' . $e->getMessage());
            if ($e->getCode() == AuthenticationException::EXPIRED_SESSION) {
                $code = IResponse::S410_GONE;
            }
            else {
                $code = IResponse::S401_UNAUTHORIZED;
            }
            $this->quitWithError($code, 'AUTH_ERROR', $e->getMessage());
        }
        catch (BadMethodCallException $e) {
            $this->quitWithError(IResponse::S405_METHOD_NOT_ALLOWED, 'INVALID_REQUEST');
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (InvalidParameterException $e) {
            $this->logger()->warn(sprintf(
                "Trying to call API (%s: %s) with invalid parameters: %s (file '%s', line %d)"
              , $requestMethod
              , sprintf('/%s/%s', $domain, $action ? $action . '/' : '')
              , $e->getMessage()
              , $e->getFile()
              , $e->getLine()
            ));
            $this->quitWithError(
                IResponse::S400_BAD_REQUEST
              , 'INVALID_PARAMETERS'
              , $e->getMessage()
              , $e->getFile()
              , $e->getLine()
            );
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (ItemNotFound $e) {
            $this->quitWithError(IResponse::S404_NOT_FOUND, 'ITEM_NOT_FOUND');
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (ServiceException $e) {
            $this->logger()->debug(sprintf(
                "API call (%s: %s) failed with %s: %s (file '%s', line %d)"
              , $requestMethod
              , sprintf('/%s/%s', $domain, $action ? $action . '/' : '')
              , $e->getErrorCode()
              , $e->getMessage() ?: "N/A"
              , $e->getFile()
              , $e->getLine()
            ));
            $this->quitWithError(
                IResponse::S400_BAD_REQUEST
              , $e->getErrorCode()
              , $e->getMessage()
              , $e->getFile()
              , $e->getLine()
            );
        }
        catch (Exception $e) {
            error_log(strval($e));
            $this->logger()->fatal($e->getMessage(), $e);
            try {
                $this->env()->em()->rollback();
            }
            /** @noinspection PhpRedundantCatchClauseInspection */
            catch (ConnectionException $ignore) {/* nevermind */}
            $this->quitWithError(
                IResponse::S500_INTERNAL_SERVER_ERROR
              , 'INTERNAL_ERROR'
              , $e->getMessage()
              , $e->getFile()
              , $e->getLine()
            );
        }
    }
}
