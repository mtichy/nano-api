# Lightweight framework for building API based on Doctrine2

API framework built above Doctrine2, providing tools for easy building REST-like API.

## Installation

via composer: 
```sh
composer require mtichy/nano-api
```
or you can just download project source files directly

## Usage

with composer: 
```php
<?php
require __DIR__.'/vendor/autoload.php';
```

without composer 
```php
<?php
require 'path-to-datetime-dir/autoload.php';
```
