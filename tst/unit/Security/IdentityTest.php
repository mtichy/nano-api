<?php

namespace MTi\UnitTest;

use MTi\Security\Identity\UsernameIdentity;
use PHPUnit\Framework\TestCase;


class IdentityTest
    extends TestCase
{
    private $fullname = 'Jarda Novotný';

    /**
     * @throws \PHPUnit\Framework\Exception
     */
    public function testData()
    {
        $i = new UsernameIdentity('tester', ['testers'], 'i');
        $i->setAttribute('fullname', $this->fullname);

        static::assertNotNull($i->getAttribute('fullname'));
        static::assertNull($i->getAttribute('lastname'));
        static::assertEquals($this->fullname, $i->getAttribute('fullname'));

        $data = $i->getAttributes();
        static::assertArrayHasKey('fullname', $data);
        static::assertEquals($this->fullname, $data['fullname']);
    }
}
