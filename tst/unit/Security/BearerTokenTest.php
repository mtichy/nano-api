<?php

namespace MTi\UnitTest;

use Darsyn\IP\Exception\InvalidIpAddressException;
use Darsyn\IP\Exception\WrongVersionException;
use Darsyn\IP\Version\Multi;
use MTi\Http\Request\HttpRequest;
use MTi\Security\AuthenticationException;
use MTi\Security\Authenticator\BearerJwtAuthenticator;
use MTi\Security\ICredentialsVerifier;


class TestCredsVerifier
    implements ICredentialsVerifier
{
    public function verifyCredentials(string $username, string $password): bool
    {
        return TRUE;
    }
}


class BearerTokenTest
    extends TestCaseBase
{
    private $secretKey = 'SeCRetKeY';
    private $user = 'tester';

    protected function setUp()
    {
        $this->env->setNow($this->now());
    }

    /**
     * @param string|NULL $token
     * @param string $ipaddr
     * @return HttpRequest
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     */
    private function request(string $token = NULL, string $ipaddr = NULL)
    {
        $emMock = $this->getMock(
            HttpRequest::class
          , ['getAuthorizationHeader', 'getIP']
        );
        $emMock->expects(self::any())
            ->method('getAuthorizationHeader')
            ->will(self::returnValue($token ? 'Bearer '.$token : NULL))
        ;
        try {
            $emMock->expects(self::any())
                ->method('getIP')
                ->will(self::returnValue(Multi::factory($ipaddr ?: '87.54.32.21')))
            ;
        }
        catch (WrongVersionException|InvalidIpAddressException $e) {
            throw new \InvalidArgumentException('Invalid IP!');
        }
        return $emMock;
    }

    /**
     * @param string|NULL $token
     * @param string|null $requestIp
     * @return BearerJwtAuthenticator
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     */
    private function authenticator(string $token = NULL, string $requestIp = NULL)
    {
        return new BearerJwtAuthenticator(
            $this->request($token, $requestIp)
          , new TestCredsVerifier()
          , $this->secretKey
          , $this->now()
        );
    }

    /**
     * @throws \PHPUnit\Framework\Exception
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     */
    public function testNotLoggedIn()
    {
        $jwt = $this->authenticator();
        $this->expectException(AuthenticationException::class);
        $jwt->getUsername();
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     * @throws \PHPUnit\Framework\Exception
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     */
    public function testLoginPass()
    {
        $jwt = $this->authenticator();
        $o = $jwt->doExplicitLogin($this->user, 'nevermind');
        self::assertTrue(is_array($o));
        self::assertArrayHasKey('token', $o);
    }

    /**
     * @throws \PHPUnit\Framework\Exception
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     */
    public function testTokenLoginFail()
    {
        $jwt = $this->authenticator('ObviouslyWrongToken');
        $this->expectException(AuthenticationException::class);
        $this->expectExceptionCode(AuthenticationException::SECURITY);
        $jwt->getUsername();
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     */
    public function testTokenLoginPass()
    {
        $jwt = $this->authenticator();
        $o = $jwt->doExplicitLogin($this->user, 'nevermind');
        $jwt = $this->authenticator($o['token']);
        $username = $jwt->getUsername();
        self::assertEquals($this->user, $username);
    }

    /**
     * @throws \Exception
     * @throws \PHPUnit\Framework\Exception
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     */
    public function testTokenLoginExpire()
    {
        $jwt = $this->authenticator();
        $jwt->setExpiration(new \DateInterval('PT1S'));
        $o = $jwt->doExplicitLogin($this->user, 'nevermind');
        $jwt = $this->authenticator($o['token']);

        usleep(1000001);

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionCode(AuthenticationException::EXPIRED_SESSION);
        $username = $jwt->getUsername();
        self::assertEquals($this->user, $username);
    }

    /**
     * @throws InvalidIpAddressException
     * @throws WrongVersionException
     * @throws \PHPUnit\Framework\AssertionFailedError
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     * @throws \ReflectionException
     */
    public function testPrivateIPCheckPass()
    {
        $validInternalIPs = [
            '10.0.0.0',
            '10.255.255.255',
            '172.16.0.0',
            '172.31.255.255',
            '192.168.0.0',
            '192.168.255.255',
        ];
        for ($i = 0; $i < count($validInternalIPs); $i++) {
            $auth = $this->authenticator(NULL, $validInternalIPs[$i]);
            $reflect = new \ReflectionClass($auth);
            $method = $reflect->getMethod('checkTokenIP');
            $method->setAccessible(TRUE);
            for ($j = 0; $j < count($validInternalIPs); $j++) {
                self::assertTrue($method->invoke($auth, Multi::factory($validInternalIPs[$j])));
            }
        }

    }
}
