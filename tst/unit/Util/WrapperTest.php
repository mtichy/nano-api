<?php

namespace MTi\UnitTest\Util;

use MTi\UnitTest\TestCaseBase;
use MTi\UnitTest\TestEnvWrapper;


class EnvWrapperTest
    extends TestCaseBase
{
    public function testWrapper()
    {
        $w = $this->env->getWrapperByName('test');
        self::assertEquals(1, TestEnvWrapper::$status);
        $w->execute(function ()
        {
            EnvWrapperTest::assertEquals(10, TestEnvWrapper::$status);
            TestEnvWrapper::$status = 5;
        });
        self::assertEquals(1, TestEnvWrapper::$status);
    }
}
