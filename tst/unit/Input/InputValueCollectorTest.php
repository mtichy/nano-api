<?php

namespace MTi\UnitTest\Input;

use MTi\IDate;
use MTi\IDatetime;
use MTi\Input\IInputValueType;
use MTi\Input\InputValueCollector;
use MTi\Input\InputValueParser;
use MTi\Input\InvalidParameterException;
use MTi\Type\ExtendedFloat;
use MTi\UnitTest\TestCaseBase;
use MTi\UnitTest\TestMoney;


class InputValueCollectorTest
    extends TestCaseBase
{
    protected function setUp()
    {
        $this->env->setNow($this->now());
    }

    private function createCollector(
        string $mode
      , array $requestData
      , array $updateDefaults = []
      , string $varnamePrefix = NULL
    )
    {
        $this->env->setupRequest($requestData);
        return new InputValueCollector(
            new InputValueParser($this->env->request(), TestMoney::class)
          , $mode
          , $this->env->getDateTimeZone()
          , TestMoney::class
          , $updateDefaults
          , $varnamePrefix
        );
    }

    /**e
     * @throws InvalidParameterException
     */
    public function testIntCollectorPass()
    {
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, ['a' => '5'])
            ->setDataType(IInputValueType::TYPE_INT)
            ->gainValue('a')
        ;
        self::assertEquals(5, $val);

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['a' => 5])
            ->setDataType(IInputValueType::TYPE_INT)
            ->gainValue('a', 'a')
        ;
        self::assertEquals(5, $val);

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['a' => NULL])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setMandatory(FALSE)
            ->gainValue('a', 'a')
        ;
        self::assertNull($val);

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['a' => 7])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setMandatory(FALSE)
            ->gainValue('a', 'a')
        ;
        self::assertEquals(7, $val);

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['a' => ''])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setMandatory(FALSE)
            ->gainValue('a')
        ;
        self::assertNull($val);

        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setMandatory(FALSE)
            ->gainValue('a')
        ;
        self::assertNull($val);

        $val = $this->createCollector(InputValueCollector::MODE_CREATION, ['a' => '654'])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setCreationDefaultValue(666)
            ->gainValue('a')
        ;
        self::assertEquals(654, $val);

        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setCreationDefaultValue(666)
            ->gainValue('a')
        ;
        self::assertEquals(666, $val);

        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setCreationDefaultValue('09')
            ->gainValue('a')
        ;
        self::assertEquals(9, $val);
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testIntCollectorFailMissing1()
    {
        $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_INT)
            ->gainValue('a')
        ;
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testIntCollectorFailMissing2()
    {
        $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setCreationDefaultValue(777)
            ->gainValue('a')
        ;
    }

    /**
     * @expectedException \InvalidArgumentException
     *
     * @throws InvalidParameterException
     */
    public function testIntCollectorFailInvalidDefault1()
    {
        $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setCreationDefaultValue('x')
            ->gainValue('a')
        ;
    }

    /**
     * @expectedException \InvalidArgumentException
     *
     * @throws InvalidParameterException
     */
    public function testIntCollectorFailInvalidDefault2()
    {
        $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setCreationDefaultValue('x')
            ->setDataType(IInputValueType::TYPE_INT)
            ->gainValue('a')
        ;
    }

    /**
     * @throws InvalidParameterException
     */
    public function testDateCollectorPass()
    {
        /** @var IDate $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, ['d' => '2019-09-15'])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->gainValue('d')
        ;
        self::assertEquals('2019-09-15', $val->isoDate());

        /** @var IDate $val */
        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['d' => $this->createDate('2019-09-16')])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->gainValue('d', 'd')
        ;
        self::assertEquals('2019-09-16', $val->isoDate());

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['d' => NULL])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->setMandatory(FALSE)
            ->gainValue('d', 'd')
        ;
        self::assertNull($val);

        /** @var IDate $val */
        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['d' => '2019-09-17'])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->setMandatory(FALSE)
            ->gainValue('d', 'd')
        ;
        self::assertEquals('2019-09-17', $val->isoDate());

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['d' => ''])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->setMandatory(FALSE)
            ->gainValue('d')
        ;
        self::assertNull($val);

        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->setMandatory(FALSE)
            ->gainValue('d')
        ;
        self::assertNull($val);

        /** @var IDate $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, ['d' => '2019-09-17'])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->setCreationDefaultValue($this->createDate('2019-09-16'))
            ->gainValue('d')
        ;
        self::assertEquals('2019-09-17', $val->isoDate());

        /** @var IDate $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->setCreationDefaultValue($this->createDate('2019-09-16'))
            ->gainValue('a')
        ;
        self::assertEquals('2019-09-16', $val->isoDate());

        /** @var IDate $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->setCreationDefaultValue('2019-09-18')
            ->gainValue('d')
        ;
        self::assertEquals('2019-09-18', $val->isoDate());
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testDateCollectorFailMissing1()
    {
        $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->gainValue('d')
        ;
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testDateCollectorFailMissing2()
    {
        $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->setCreationDefaultValue('2020-06-01')
            ->gainValue('a')
        ;
    }

    /**
     * @throws InvalidParameterException
     */
    public function testDatetimeCollectorPass()
    {
        /** @var IDatetime $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, ['dt' => '2019-09-15T12:00:00'])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->gainValue('dt')
        ;
        self::assertEquals('2019-09-15 12:00:00', $val->isoDatetime());

        /** @var IDatetime $val */
        $val = $this
            ->createCollector(
                InputValueCollector::MODE_UPDATE_PART
              , []
              , ['dt' => $this->createDatetime('2019-09-16 12:00:00')]
            )
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->gainValue('dt', 'dt')
        ;
        self::assertEquals('2019-09-16 12:00:00', $val->isoDatetime());

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['dt' => NULL])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->setMandatory(FALSE)
            ->gainValue('dt', 'dt')
        ;
        self::assertNull($val);

        /** @var IDatetime $val */
        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['dt' => '2019-09-17 12:00:00'])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->setMandatory(FALSE)
            ->gainValue('dt', 'dt')
        ;
        self::assertEquals('2019-09-17 12:00:00', $val->isoDatetime());

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['d' => ''])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->setMandatory(FALSE)
            ->gainValue('d')
        ;
        self::assertNull($val);

        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->setMandatory(FALSE)
            ->gainValue('dt')
        ;
        self::assertNull($val);

        /** @var IDatetime $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, ['dt' => '2019-09-17T12:00:00'])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->setCreationDefaultValue($this->createDatetime('2019-09-16 12:00:00'))
            ->gainValue('dt')
        ;
        self::assertEquals('2019-09-17 12:00:00', $val->isoDatetime());

        /** @var IDatetime $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->setCreationDefaultValue($this->createDatetime('2019-09-16 12:00:00'))
            ->gainValue('a')
        ;
        self::assertEquals('2019-09-16 12:00:00', $val->isoDatetime());

        /** @var IDatetime $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->setCreationDefaultValue('2019-09-18 12:00:00')
            ->gainValue('d')
        ;
        self::assertEquals('2019-09-18 12:00:00', $val->isoDatetime());
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testDatetimeCollectorFailMissing1()
    {
        $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->gainValue('dt')
        ;
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testDatetimeCollectorFailMissing2()
    {
        $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [])
            ->setDataType(IInputValueType::TYPE_DATETIME)
            ->setCreationDefaultValue('2020-06-01 12:00:00')
            ->gainValue('dt')
        ;
    }

    /**
     * @throws InvalidParameterException
     */
    public function testFloatCollectorPass()
    {
        /** @var ExtendedFloat $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, ['f' => '8.0'])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->gainValue('f')
        ;
        self::assertEquals(8, $val->float());

        /** @var ExtendedFloat $val */
        $val = $this
            ->createCollector(
                InputValueCollector::MODE_UPDATE_PART
              , []
              , ['fd' => ExtendedFloat::fromFloat(9.999)]
            )
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->gainValue('f', 'fd')
        ;
        self::assertEquals(9.999, $val->float());

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['fd' => NULL])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setMandatory(FALSE)
            ->gainValue('f', 'fd')
        ;
        self::assertNull($val);

        /** @var ExtendedFloat $val */
        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [], ['fd' => 0.5])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setMandatory(FALSE)
            ->gainValue('f', 'fd')
        ;
        self::assertEquals(0.5, $val->float());

        $val = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['f' => ''], ['fd' => ExtendedFloat::fromFloat(1.0)])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setMandatory(FALSE)
            ->gainValue('f', 'fd')
        ;
        self::assertNull($val);

        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setMandatory(FALSE)
            ->gainValue('f')
        ;
        self::assertNull($val);

        /** @var ExtendedFloat $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, ['f' => '4.33'])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setCreationDefaultValue(7.99)
            ->gainValue('f')
        ;
        self::assertEquals(4.33, $val->float());

        /** @var ExtendedFloat $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setCreationDefaultValue(0.43)
            ->gainValue('f')
        ;
        self::assertEquals(0.43, $val->float());

        /** @var ExtendedFloat $val */
        $val = $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setCreationDefaultValue(5.67)
            ->gainValue('f')
        ;
        self::assertEquals(5.67, $val->float());
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testFloatCollectorFailMissing1()
    {
        $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->gainValue('f')
        ;
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testFloatCollectorFailMissing2()
    {
        $this->createCollector(InputValueCollector::MODE_UPDATE_PART, [])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setCreationDefaultValue('3.56')
            ->gainValue('f')
        ;
    }

    /**
     * @expectedException \InvalidArgumentException
     *
     * @throws InvalidParameterException
     */
    public function testFloatCollectorFailInvalidDefault()
    {
        $this->createCollector(InputValueCollector::MODE_CREATION, [])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setCreationDefaultValue('x')
            ->gainValue('f')
        ;
    }

    /**
     * @param string $inputValue
     * @param bool $null
     * @return mixed
     * @throws InvalidParameterException
     */
    private function getNullifResult(string $inputValue, bool $null)
    {
        return $this->createCollector(InputValueCollector::MODE_CREATION, ['a' => $inputValue])
            ->setNullIf(function () use ($null)
            {
                return $null;
            })
            ->gainValue('a')
        ;
    }

    /**
     * @throws InvalidParameterException
     */
    public function testNullif()
    {
        self::assertEquals('NOTNULL', $this->getNullifResult('NOTNULL', false));

        self::assertNull($this->getNullifResult('NOTNULL', true));
    }

    /**
     * @throws InvalidParameterException
     */
    public function testImmutablePass()
    {
        $valI = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['a' => '4'], ['ad' => 6])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setImmutableValues([4])
            ->gainValue('a', 'ad')
        ;
        self::assertEquals(4, $valI);

        /** @var ExtendedFloat $valF */
        $valF = $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['a' => '5.7'], ['ad' => ExtendedFloat::fromFloat(6)])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setImmutableValues([4.2])
            ->gainValue('a', 'ad')
        ;
        self::assertEquals(5.7, $valF->float());
    }

    /**
     * @expectedException \InvalidArgumentException
     *
     * @throws InvalidParameterException
     */
    public function testImmutableFailInvalidDef()
    {
        $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['a' => '4'], ['ad' => 6])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setImmutableValues(['H'])
            ->gainValue('a', 'ad')
        ;
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -5
     */
    public function testImmutableFailImmutableInt()
    {
        $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['a' => '7'], ['ad' => 6])
            ->setDataType(IInputValueType::TYPE_INT)
            ->setImmutableValues(['06'])
            ->gainValue('a', 'ad')
        ;
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -5
     */
    public function testImmutableFailImmutableBool()
    {
        $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['a' => '0'], ['ad' => TRUE])
            ->setDataType(IInputValueType::TYPE_BOOLEAN)
            ->setImmutableValues(['1'])
            ->gainValue('a', 'ad')
        ;
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -5
     */
    public function testImmutableFailImmutableFloat()
    {
        $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['a' => '1'], ['ad' => 0.1])
            ->setDataType(IInputValueType::TYPE_FLOAT)
            ->setImmutableValues(['0.1'])
            ->gainValue('a', 'ad')
        ;
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -5
     */
    public function testImmutableFailImmutableDate()
    {
        $this->createCollector(InputValueCollector::MODE_UPDATE_PART, ['a' => '2018-01-01'], ['ad' => '2000-01-01'])
            ->setDataType(IInputValueType::TYPE_DATE)
            ->setImmutableValues(['2000-01-01'])
            ->gainValue('a', 'ad')
        ;
    }
}
