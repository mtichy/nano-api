<?php

namespace MTi\UnitTest;

use MTi\DateTime\JulianDate;
use MTi\Input\ArrayInputDataProvider;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;


class ArrayInputDataProviderTest
    extends TestCaseBase
{

    private function data()
    {
        try {
            return [
                'a' => 'A',
                'b' => 1,
                'c' => new JulianDate(1, 1, 2000),
                'h' => '<i>h</i>',
            ];
        }
        catch (InvalidDateException | UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    private function dataProvider()
    {
        return new ArrayInputDataProvider($this->data());
    }


    /**
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testSecureData()
    {
        self::assertEquals('A', $this->dataProvider()->getSecureData('a'));
        self::assertEquals(1, $this->dataProvider()->getSecureData('b'));
        self::assertEquals('h', $this->dataProvider()->getSecureData('h'));
    }

    /**
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testUnsecureData()
    {
        self::assertEquals('A', $this->dataProvider()->getUnsecureData('a'));
        self::assertEquals(1, $this->dataProvider()->getUnsecureData('b'));
        self::assertEquals('2000-01-01', $this->dataProvider()->getUnsecureData('c')->isoDate());
    }


    /**
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testUnsecureDataMissing()
    {
        self::assertNull($this->dataProvider()->getUnsecureData('q'));
    }
    
    /**
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testSecureDataMissing()
    {
        self::assertNull($this->dataProvider()->getSecureData('q'));
    }


}
