<?php

namespace MTi\UnitTest;

use MTi\DateTime\ExtendedDatetime;
use MTi\DateTime\JulianDate;
use MTi\IDate;
use MTi\IDatetime;
use MTi\Input\InputValueParser;
use MTi\Type\ExtendedFloat;
use MTi\Type\Money;
use MTi\UnsupportedDateTimeException;


class InputValueParserTest
    extends TestCaseBase
{
    private function parser(array $fakeData)
    {
        $this->env->setupRequest($fakeData);
        try {
            $this->env->setNow(
                new ExtendedDatetime('2018-11-12 08:30:00', new \DateTimeZone('UTC'))
              , TRUE
            );
        }
        catch (UnsupportedDateTimeException $e) {
            throw new \LogicException();
        }
        return new InputValueParser($this->env->request(), TestMoney::class);
    }

    public function initializationPassProviderBigint()
    {
        return [
            'positive_max' => ['12345678901234567890', '12345678901234567890'],
            'zero' => ['0', '0'],
            'negative_max' => ['-12345678901234567890', '-12345678901234567890'],
        ];
    }

    public function initializationPassProviderInt()
    {
        return [
            'positive_max' => [PHP_INT_MAX, strval(PHP_INT_MAX)],
            'zero' => [0, '0'],
            'negative_max' => [PHP_INT_MIN, strval(PHP_INT_MIN)],
        ];
    }

    public function initializationPassProviderFloat()
    {
        return [
            'positive1' => [0.0, '0.00000000010', 5],
            'positive2' => [0.0000000001, '0.00000000010', 10],
            'positive3' => [1.233333, '1.233333', 10],
            'positive4' => [1.233, '1.233333', 3],
            'positive5' => [456789, '456789', 0],
            'zero' => [0.0, '0', 3],
            'zerodot' => [0.0, '0.0', 3],
            'negative1' => [-0.99999, '-0.99999', 5],
            'negative2' => [-111111111111, '-111111111111', 20],
        ];
    }

    public function initializationPassProviderDate()
    {
        return [
            'zero' => ['2000-01-01'],
            'date' => ['2018-06-15'],
            'infinity' => ['9999-12-31'],
        ];
    }

    public function initializationPassProviderDatetime()
    {
        return [
            'zero' => ['2000-01-01 00:00:00', '2000-01-01T00:00:00'],
            'date' => ['2018-06-15 07:50:14', '2018-06-15T07:50:14'],
            'infinity' => ['9999-12-31 23:59:59', '9999-12-31T23:59:59'],
        ];
    }


    /**
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testExpectedFormatString()
    {
        $string = 'žluťoučký KŮŇ šťastně RŽÁL';
        self::assertEquals($string, $this->parser(['x' => $string])->getString('x'));
    }

    /**
     * @dataProvider initializationPassProviderBigint
     *
     * @param $expected
     * @param $input
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testExpectedFormatBigint($expected, $input)
    {
        self::assertEquals($expected, $this->parser(['x' => $input])->getBigInt('x'));
    }

    /**
     * @dataProvider initializationPassProviderInt
     *
     * @param $expected
     * @param $input
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testExpectedFormatInt($expected, $input)
    {
        self::assertEquals($expected, $this->parser(['x' => $input])->getInt('x'));
    }

    /**
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testExpectedFormatPositiveInt()
    {
        self::assertEquals(PHP_INT_MAX, $this->parser(['x' => PHP_INT_MAX])->getInt('x'));
    }

    /**
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testExpectedFormatBool()
    {
        self::assertTrue($this->parser(['x' => 'true'])->getBool('x'));
        self::assertTrue($this->parser(['x' => 'TRUE'])->getBool('x'));
        self::assertTrue($this->parser(['x' => '1'])->getBool('x'));
        self::assertFalse($this->parser(['x' => 'false'])->getBool('x'));
        self::assertFalse($this->parser(['x' => 'FALSE'])->getBool('x'));
        self::assertFalse($this->parser(['x' => '0'])->getBool('x'));
    }

    /**
     * @dataProvider initializationPassProviderFloat
     *
     * @param $expected
     * @param $input
     * @param $decimals
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     * @throws \PHPUnit\Framework\Exception
     */
    public function testExpectedFormatFloat($expected, $input, $decimals)
    {
        $v = $this->parser(['x' => $input])->getFloat('x', NULL, $decimals);
        self::assertInstanceOf(ExtendedFloat::class, $v);
        self::assertEquals($expected, $v->float(),  NULL,0.00000001);
    }

    /**
     * @dataProvider initializationPassProviderFloat
     *
     * @param $expected
     * @param $input
     * @param $decimals
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     * @throws \PHPUnit\Framework\Exception
     */
    public function testExpectedFormatMoney($expected, $input, $decimals)
    {
        $v = $this->parser(['x' => $input])->getMoney('x', NULL, $decimals);
        self::assertInstanceOf(Money::class, $v);
        self::assertEquals($expected, $v->float(),  NULL,0.00000001);
    }

    /**
     * @dataProvider initializationPassProviderDate
     *
     * @param $input
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     * @throws \PHPUnit\Framework\Exception
     */
    public function testExpectedFormatDate($input)
    {
        $v = $this->parser(['x' => $input])->getDate('x');
        self::assertInstanceOf(IDate::class, $v);
        self::assertEquals($input, $v->isoDate());
    }

    /**
     * @dataProvider initializationPassProviderDatetime
     *
     * @param $expected
     * @param $input
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     * @throws \PHPUnit\Framework\Exception
     */
    public function testExpectedFormatDatetime($expected, $input)
    {
        $v = $this->parser(['x' => $input])->getDateTime('x', $this->env->getDateTimeZone());
        self::assertInstanceOf(IDatetime::class, $v);
        self::assertEquals($expected, $v->isoDatetime());
    }

    /**
     * @throws UnsupportedDateTimeException
     * @throws \MTi\Input\EmptyParameterException
     * @throws \MTi\Input\InvalidParameterException
     * @throws \MTi\InvalidDateException
     * @throws \MTi\InvalidNumberException
     * @throws \MTi\UnsupportedDateException
     */
    public function testDefaults()
    {
        self::assertEquals('Qwerty', $this->parser([])->getString('x', 'Qwerty'));
        self::assertEquals(5000, $this->parser([])->getInt('x', 5000));
        self::assertEquals(3333, $this->parser([])->getPositiveInt('x', 3333));
        $bigint = strval(PHP_INT_MAX).'0';
        self::assertEquals($bigint, $this->parser([])->getBigInt('x', $bigint));
        $floatDefault = new TestMoney(789.123, 3);
        self::assertEquals(789.123, $this->parser([])->getFloat('x', $floatDefault)->float());
        self::assertEquals(789.123, $this->parser([])->getMoney('x', $floatDefault)->float());
        self::assertEquals('2020-12-05', $this->parser([])->getDate('x', new JulianDate('2020-12-05'))->isoDate());
        $tz = $this->env->getDateTimeZone();
        self::assertEquals(
            '2020-12-05 12:00:40'
          , $this->parser([])->getDateTime('x', $tz, new ExtendedDatetime('2020-12-05 12:00:40', $tz))->isoDatetime()
        );
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testStringFailMissing()
    {
        $this->parser([])->getString('x');
    }

    /**
     * @expectedException \MTi\Input\EmptyParameterException
     *
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testStringFailEmpty()
    {
        $this->parser(['x' => ''])->getString('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testBigIntFailMissing()
    {
        $this->parser([])->getBigInt('x');
    }

    /**
     * @expectedException \MTi\Input\EmptyParameterException
     *
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testBigIntFailEmpty()
    {
        $this->parser(['x' => ''])->getBigInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testBigIntFailLetter()
    {
        $this->parser(['x' => '123A56789'])->getBigInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testIntFailMissing()
    {
        $this->parser([])->getInt('x');
    }

    /**
     * @expectedException \MTi\Input\EmptyParameterException
     *
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testIntFailEmpty()
    {
        $this->parser(['x' => ''])->getInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testIntFailLetter()
    {
        $this->parser(['x' => '123A56789'])->getInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testIntFailOverflow()
    {
        $this->parser(['x' => strval(PHP_INT_MAX).'5'])->getInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testPositiveIntFailMissing()
    {
        $this->parser([])->getPositiveInt('x');
    }

    /**
     * @expectedException \MTi\Input\EmptyParameterException
     *
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testPositiveIntFailEmpty()
    {
        $this->parser(['x' => ''])->getPositiveInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testPositiveIntFailLetter()
    {
        $this->parser(['x' => '123A56789'])->getPositiveInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testPositiveIntFailZero()
    {
        $this->parser(['x' => '0'])->getPositiveInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testPositiveIntFailNegative()
    {
        $this->parser(['x' => '-5'])->getPositiveInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testPositiveIntFailOverflow()
    {
        $this->parser(['x' => strval(PHP_INT_MAX).'0'])->getPositiveInt('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testBoolFailMissing()
    {
        $this->parser([])->getBool('x');
    }

    /**
     * @expectedException \MTi\Input\EmptyParameterException
     *
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testBoolFailEmpty()
    {
        $this->parser(['x' => ''])->getBool('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testBoolFailValue()
    {
        $this->parser(['x' => 't'])->getBool('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testFloatFailMissing()
    {
        $this->parser([])->getFloat('x');
    }

    /**
     * @expectedException \MTi\Input\EmptyParameterException
     *
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testFloatFailEmpty()
    {
        $this->parser(['x' => ''])->getFloat('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testFloatFailLetter()
    {
        $this->parser(['x' => '123.7a'])->getFloat('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testFloatFailCompletePre()
    {
        $this->parser(['x' => '.55'])->getFloat('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testFloatFailCompletePost()
    {
        $this->parser(['x' => '55.'])->getFloat('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testMoneyFailMissing()
    {
        $this->parser([])->getMoney('x');
    }

    /**
     * @expectedException \MTi\Input\EmptyParameterException
     *
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testMoneyFailEmpty()
    {
        $this->parser(['x' => ''])->getMoney('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testMoneyFailLetter()
    {
        $this->parser(['x' => '123.7a'])->getMoney('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testMoneyFailCompletePre()
    {
        $this->parser(['x' => '.55'])->getMoney('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testMoneyFailCompletePost()
    {
        $this->parser(['x' => '55.'])->getMoney('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testDateFailMissing()
    {
        $this->parser([])->getDate('x');
    }

    /**
     * @expectedException \MTi\Input\EmptyParameterException
     *
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testDateFailEmpty()
    {
        $this->parser(['x' => ''])->getDate('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testDateFailFormat1()
    {
        $this->parser(['x' => '2016-2a-01'])->getDate('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testDateFailFormat2()
    {
        $this->parser(['x' => '2016/12/01'])->getDate('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testDateFailMonths()
    {
        $this->parser(['x' => '2016-13-01'])->getDate('x');
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -2
     */
    public function testDatetimeFailMissing()
    {
        $this->parser([])->getDateTime('x', $this->env->getDateTimeZone());
    }

    /**
     * @expectedException \MTi\Input\EmptyParameterException
     *
     * @throws \MTi\Input\InvalidParameterException
     */
    public function testDatetimeFailEmpty()
    {
        $this->parser(['x' => ''])->getDateTime('x', $this->env->getDateTimeZone());
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testDatetimeFailFormat1()
    {
        $this->parser(['x' => '2016-2a-01T23:48:20'])->getDateTime('x', $this->env->getDateTimeZone());
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testDatetimeFailFormat2()
    {
        $this->parser(['x' => '2016-12-01T01:2x:48'])->getDateTime('x', $this->env->getDateTimeZone());
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testDatetimeFailMonths()
    {
        $this->parser(['x' => '2016-13-01T00:00:00'])->getDateTime('x', $this->env->getDateTimeZone());
    }

    /**
     * @expectedException \MTi\Input\InvalidParameterException
     * @expectedExceptionCode -3
     */
    public function testDatetimeFailMinutes()
    {
        $this->parser(['x' => '2016-01-01 00:61:00'])->getDateTime('x', $this->env->getDateTimeZone());
    }
}
