<?php

use Doctrine\Common\Annotations\AnnotationRegistry;

require_once __DIR__.'/../common/TestMoney.php';
require_once __DIR__.'/../common/TestEnv.php';
require_once __DIR__ . '/../common/TestEnvWrapper.php';


$bootstrapConfig = \MTi\Application\BootstrapConfig::createConfig()
    ->setEnvClass(\MTi\UnitTest\TestEnv::class)
;

if ($bootstrapConfig->useContainer) {
    $container = new \Symfony\Component\DependencyInjection\ContainerBuilder();
}

define('PROJ_ROOT', realpath(__DIR__ . '/../../'));

error_reporting(E_ALL | E_STRICT);
date_default_timezone_set('Europe/Prague');
ini_set('display_errors', $bootstrapConfig->displayErrors);

require_once PROJ_ROOT . '/vendor/autoload.php';
\MTi\Util\Autoloader::autoloadDirs([PROJ_ROOT.'/src'], sys_get_temp_dir(), $container);

$cfg = new \MTi\Config\TypedConfig([]);

$definition = new \Symfony\Component\DependencyInjection\Definition($bootstrapConfig->envClass);
$definition
    ->setAutowired(TRUE)
    ->setAutoconfigured(TRUE)
    ->setPublic(TRUE)
;
if ($bootstrapConfig->envParameters) {
    if (is_array($bootstrapConfig->envParameters)) {
        foreach ($bootstrapConfig->envParameters as $parameter) {
            $definition->addArgument($parameter);
        }
    }
    else {
        $definition->addArgument($bootstrapConfig->envParameters);
    }
}
else {
    $definition->addArgument($cfg)
        ->addArgument('UTF8')
    ;
}
if ($bootstrapConfig->envExtraParameters) {
    if (is_array($bootstrapConfig->envExtraParameters)) {
        foreach ($bootstrapConfig->envExtraParameters as $parameter) {
            $definition->addArgument($parameter);
        }
    }
    else {
        $definition->addArgument($bootstrapConfig->envExtraParameters);
    }
}

$container->setDefinition('environment', $definition);
/** @var \MTi\IEnv $environment */
try {
    $environment = $container->get('environment');
}
catch (Exception $e) {
    throw new LogicException();
}

require_once __DIR__.'/../common/TestCaseBase.php';
\MTi\UnitTest\TestCaseBase::$container = $container;

AnnotationRegistry::registerLoader('class_exists');

