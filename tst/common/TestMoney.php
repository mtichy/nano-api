<?php

namespace MTi\UnitTest;

use MTi\Type\Money;


class TestMoney
    extends Money
{
    protected function withCurrency(string $v): string
    {
        return '$'.$v;
    }
}
