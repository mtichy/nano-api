<?php

namespace MTi\UnitTest;

use Doctrine\ORM\EntityManager;
use MTi\Config\DotCascadedConfig;
use MTi\Http\IRequest;
use MTi\Http\IResponse;
use MTi\Http\Request\TestRequest;
use MTi\Http\Response\HttpResponse;
use MTi\IConfig;
use MTi\IDate;
use MTi\IDatetime;
use MTi\IEnv;
use MTi\Security\Authenticator\WelcomeAllAuthenticator;
use MTi\Security\IAuthenticator;
use MTi\Security\Identity\UsernameIdentity;
use MTi\Security\IIdentity;
use MTi\Util\EnvWrapper;
use Nette\Caching\Cache;
use Nette\Caching\Storages\DevNullStorage;
use Nette\NotImplementedException;


class TestEnv
    implements IEnv
{
    public function __construct(IConfig $config)
    {
        $this->_config = $config;
    }

    private $_config;
    private $_request;
    private $_today;
    /** @var IDatetime */
    private $_now;

    public function setupRequest(array $fakeData, string $method = 'GET', $contentType = IRequest::CONTENT_TYPE_JSON): TestEnv
    {
        $this->_request = new TestRequest($fakeData, $method, $contentType);
        return $this;
    }

    public function setToday(IDate $today): TestEnv
    {
        $this->_today = $today;
        return $this;
    }

    public function setNow(IDatetime $now, bool $updateToday = FALSE): TestEnv
    {
        $this->_now = $now;
        if ($updateToday) {
            $this->setToday($now->date());
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function config(string $section = NULL): IConfig
    {
        if (is_null($section)) {
            return $this->_config;
        }
        else {
            return new DotCascadedConfig($this->_config->need($section));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function request(): IRequest
    {
        return $this->_request;
    }

    /**
     * {@inheritdoc}
     */
    public function response(): IResponse
    {
        // TODO: fake response
        if (!$this->_response) {
            $this->_response = new HttpResponse($this->logger(), true);
        }
        return $this->_response;
    }
    private $_response;

    /**
     * @param string $name
     * @return \Logger
     */
    public function logger(string $name = NULL): \Logger
    {
        return \Logger::getLogger('void');
    }

    /**
     * {@inheritdoc}
     */
    public function em(): EntityManager
    {
        throw new NotImplementedException();
    }

    /**
     * {@inheritdoc}
     */
    public function currentDate(): IDate
    {
        if (!$this->_today) {
            throw new \LogicException('Today not set.');
        }
        return $this->_today;
    }

    /**
     * {@inheritdoc}
     */
    public function currentDatetime(string $tzName = NULL): IDatetime
    {
        if (!$this->_now) {
            throw new \LogicException('Now not set.');
        }
        return $this->_now;
    }

    /**
     * {@inheritdoc}
     */
    public function getDateTimeZone(string $tzName = NULL): \DateTimeZone
    {
        if (!$this->_now) {
            throw new \LogicException('Now not set.');
        }
        return $this->_now->getTimeZone();
    }

    /**
     * @return IAuthenticator
     */
    public function authenticator(): IAuthenticator
    {
        return new WelcomeAllAuthenticator();
    }

    /**
     * {@inheritdoc}
     */
    public function user(): IIdentity
    {
        if (!$this->_ui) {
            $this->_ui = new UsernameIdentity('UnitTester', ['tester'], 'test');
        }
        return $this->_ui;
    }
    private $_ui;

    /**
     * {@inheritdoc}
     */
    public function cacheDir(): string
    {
        return sys_get_temp_dir();
    }

    /**
     * {@inheritdoc}
     */
    public function logDir(): string
    {
        return sys_get_temp_dir();
    }

    /**
     * {@inheritdoc}
     */
    public function proxyDir(): string
    {
        return sys_get_temp_dir();
    }

    /**
     * {@inheritdoc}
     */
    public function cache(string $namespace = ''): Cache
    {
        return new Cache(new DevNullStorage(), $namespace);
    }

    /**
     * {@inheritdoc}
     */
    public function stage(): string
    {
        return 'UnitTest';
    }

    /**
     * {@inheritdoc}
     */
    public function isProduction(): bool
    {
        return FALSE;
    }

    /**
     * {@inheritdoc}
     */
    public function isDevelopment(): bool
    {
        return TRUE;
    }

    /**
     * {@inheritdoc}
     */
    public function moneyClassName(): string
    {
        return TestMoney::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getWrapperByName(string $name): EnvWrapper
    {
        if ($name === 'test') {
            return new TestEnvWrapper($this);
        }
        throw new \MTi\NotImplementedException();
    }

    /**
     * {@inheritdoc}
     */
    public function isInMaintenance(): bool
    {
        return FALSE;
    }
}
