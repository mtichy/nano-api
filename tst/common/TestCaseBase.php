<?php

namespace MTi\UnitTest;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use MTi\DateTime\ExtendedDatetime;
use MTi\DateTime\JulianDate;
use MTi\Http\IRequest;
use MTi\IDate;
use MTi\IDatetime;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;
use MTi\UnsupportedDateTimeException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;


class TestCaseBase
    extends TestCase
{
    /** @var ContainerBuilder */
    static $container;

    public function __construct(
        $name = NULL
      , array $data = []
      , $dataName = ''
    )
    {
        parent::__construct($name, $data, $dataName);
        try {
            $this->env = self::$container->get('environment');
        }
        catch (\Exception $e) {
            throw new \LogicException();
        }
    }

    /** @var \MTi\UnitTest\TestEnv */
    protected $env;

    protected function getMock(string $className, array $methods)
    {
        return $this->getMockBuilder($className)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->setMethods($methods)
            ->getMock()
        ;
    }

    /**
     * @param ObjectRepository|null $fakeRepo
     * @return EntityManager
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     */
    protected function getEmMock(ObjectRepository $fakeRepo = NULL)
    {
        $emMock = $this->getMock(
            '\Doctrine\ORM\EntityManager'
          , ['getRepository', 'getClassMetadata', 'persist', 'flush']
        );
        if ($fakeRepo) {
            $emMock->expects(self::any())
                ->method('getRepository')
                ->will(self::returnValue($fakeRepo));
        }
        $emMock->expects(self::any())
            ->method('getClassMetadata')
            ->will(self::returnValue(
                $this->getMock(
                    '\Doctrine\ORM\Mapping\ClassMetadata'
                  , []
                )
            ))
        ;
        $emMock->expects(self::any())
            ->method('persist')
            ->will(self::returnValue(NULL))
        ;
        $emMock->expects(self::any())
            ->method('flush')
            ->will(self::returnValue(NULL))
        ;
        return $emMock;
    }

    /**
     * @param $repository
     * @param $repositoryName
     * @param $repositoryMethod
     * @param $repositoryMethodReturnVal
     * @return \PHPUnit\Framework\MockObject\MockObject
     * @throws \PHPUnit\Framework\MockObject\RuntimeException
     */
    protected function createLoadedMockedDoctrineRepository(
        $repository
      , $repositoryName
      , $repositoryMethod
      , $repositoryMethodReturnVal
    )
    {
        $mockEM = $this->getMock(
            '\Doctrine\ORM\EntityManager'
          , ['getRepository', 'getClassMetadata', 'persist', 'flush']
        );
        $mockSVRepo = $this->getMock($repository, [$repositoryMethod]);

        $mockEM
            ->expects(self::any())
            ->method('getClassMetadata')
            ->will(self::returnValue((object)['name' => 'aClass']))
        ;
        $mockEM
            ->expects(self::any())
            ->method('persist')
            ->will(self::returnValue(null))
        ;
        $mockEM
            ->expects(self::any())
            ->method('flush')
            ->will(self::returnValue(null))
        ;

        $mockSVRepo
            ->expects(self::once())
            ->method($repositoryMethod)
            ->will(self::returnValue($repositoryMethodReturnVal))
        ;
        $mockEM
            ->expects(self::once())
            ->method('getRepository')
            ->with($repositoryName)
            ->will(self::returnValue($mockSVRepo))
        ;
        return $mockEM;
    }


    protected function accessObject(object $object, \Closure $function)
    {
        $closure = $function;
        $doClosure = $closure->bindTo($object, get_class($object));
        $doClosure();
    }

    protected function createDate(string $dateString): IDate
    {
        try {
            return new JulianDate($dateString);
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    protected function createDatetime(string $datetimeString): IDatetime
    {
        try {
            return new ExtendedDatetime($datetimeString, $this->timezone());
        }
        catch (UnsupportedDateTimeException $e) {
            throw new \LogicException();
        }
    }

    protected function timezone(): \DateTimeZone
    {
        return new \DateTimeZone('UTC');
    }

    protected function nowISO(): string
    {
        return '2000-01-01 00:00:00';
    }

    final protected function now(): IDatetime
    {
        try {
            return new ExtendedDatetime($this->nowISO(), $this->timezone());
        }
        catch (UnsupportedDateTimeException $e) {
            throw new \LogicException();
        }
    }

    final protected function today(): IDate
    {
        return $this->now()->date();
    }

    final protected function em(): EntityManager
    {
        return $this->env->em();
    }

    final protected function setupEnv(array $requestData, string $method = 'GET', $contentType = IRequest::CONTENT_TYPE_JSON)
    {
        $this->env
            ->setupRequest($requestData, $method, $contentType)
            ->setNow($this->now(), TRUE)
        ;
    }
}
