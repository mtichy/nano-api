<?php

namespace MTi\UnitTest;

use MTi\Util\EnvWrapper;


class TestEnvWrapper
    extends EnvWrapper
{
    public static $status = 1;

    protected function before(): void
    {
        self::$status = 10;
    }

    protected function after(): void
    {
        self::$status = 1;
    }
}
